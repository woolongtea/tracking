/*************************************************************************
 *                                                                       *
 * Vega FEM Simulation Library Version 1.1                               *
 *                                                                       *
 * "corotational linear FEM" library , Copyright (C) 2012 USC            *
 * All rights reserved.                                                  *
 *                                                                       *
 * Code author: Jernej Barbic                                            *
 * http://www.jernejbarbic.com/code                                      *
 *                                                                       *
 * Research: Jernej Barbic, Fun Shing Sin, Daniel Schroeder,             *
 *           Doug L. James, Jovan Popovic                                *
 *                                                                       *
 * Funding: National Science Foundation, Link Foundation,                *
 *          Singapore-MIT GAMBIT Game Lab,                               *
 *          Zumberge Research and Innovation Fund at USC                 *
 *                                                                       *
 * This library is free software; you can redistribute it and/or         *
 * modify it under the terms of the BSD-style license that is            *
 * included with this library in the file LICENSE.txt                    *
 *                                                                       *
 * This library is distributed in the hope that it will be useful,       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the file     *
 * LICENSE.TXT for more details.                                         *
 *                                                                       *
 *************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//#include <pthread.h>
#include <vector>
#include <set>
#include "include\macros.h"
#include "corotationalLinearFEMMT.h"
//#include "performanceCounter\performanceCounter.h"

#include "ThreadPool.h"

using namespace std;

CorotationalLinearFEMMT::CorotationalLinearFEMMT(TetMesh * tetMesh, int numThreads_)
    : CorotationalLinearFEM(tetMesh)
    , numThreads(numThreads_)
    , threadArgv(NULL)
{
    Initialize();
}

CorotationalLinearFEMMT::~CorotationalLinearFEMMT()
{
    free(startElement);
    free(endElement);
    free(internalForceBuffer);
    for(int i=0; i<numThreads; i++)
        delete(stiffnessMatrixBuffer[i]);
    free(stiffnessMatrixBuffer);
    free(threadArgv);
}

void CorotationalLinearFEMMT::Initialize()
{
    internalForceBuffer = (double*) malloc (sizeof(double) * numThreads * 3 * tetMesh->getNumVertices());

    // generate skeleton matrices
    stiffnessMatrixBuffer = (SparseMatrix**) malloc (sizeof(SparseMatrix*) * numThreads);

    SparseMatrix * sparseMatrix;
    GetStiffnessMatrixTopology(&sparseMatrix);
    for(int i=0; i<numThreads; i++)
      stiffnessMatrixBuffer[i] = new SparseMatrix(*sparseMatrix);

    // split the workload
    int numElements = tetMesh->getNumElements();
    startElement = (int*) malloc (sizeof(int) * numThreads);
    endElement = (int*) malloc (sizeof(int) * numThreads);

    int remainder = numElements % numThreads;
    // the first 'remainder' nodes will process one edge more
    int jobSize = numElements / numThreads;

    int rest = numElements;
    for(int rank=0; rank < numThreads; rank++)
    {
        if (rank == 0)
            startElement[rank] = 0;
        else
            startElement[rank] = endElement[rank - 1];

        int nJob = rest / (numThreads - rank);
        endElement[rank] = startElement[rank] + nJob;
        rest -= nJob;
      //if (rank < remainder)
      //{
      //  startElement[rank] = rank * (jobSize+1);
      //  endElement[rank] = (rank+1) * (jobSize+1);
      //}
      //else
      //{
      //  startElement[rank] = remainder * (jobSize+1) + (rank-remainder) * jobSize;
      //  endElement[rank] = remainder * (jobSize+1) + ((rank-remainder)+1) * jobSize;
      //}
    }

    delete sparseMatrix;

    threadArgv = (struct CorotationalLinearFEMMT_threadArg*) malloc (sizeof(struct CorotationalLinearFEMMT_threadArg) * numThreads);
    //pthread_t * tid = (pthread_t*) malloc (sizeof(pthread_t) * numThreads);
    
    int numVertices3 = 3 * tetMesh->getNumVertices();
    
    for(int i=0; i<numThreads; i++)
    {
       threadArgv[i].corotationalLinearFEMMT = this;
       //threadArgv[i].u = u;
       threadArgv[i].f = &internalForceBuffer[i * numVertices3];
       threadArgv[i].stiffnessMatrix = stiffnessMatrixBuffer[i];
       //threadArgv[i].warp = warp;
       threadArgv[i].rank = i;
    }


    printf("Total elements: %d \n", numElements);
    printf("Num threads: %d \n", numThreads);
    printf("Canonical job size: %d \n", jobSize);
    printf("Num threads with job size augmented by one edge: %d \n", remainder);
}

void CorotationalLinearFEMMT_WorkerThread(void * arg)
{
    CorotationalLinearFEMMT::CorotationalLinearFEMMT_threadArg * threadArgp 
        = (CorotationalLinearFEMMT::CorotationalLinearFEMMT_threadArg*) arg;

    CorotationalLinearFEMMT * corotationalLinearFEMMT = threadArgp->corotationalLinearFEMMT;
    double * p = threadArgp->p;
    double * f = threadArgp->f;
    SparseMatrix * stiffnessMatrix = threadArgp->stiffnessMatrix;
    int warp = threadArgp->warp;
    int rank = threadArgp->rank;
    int startElement = corotationalLinearFEMMT->GetStartElement(rank);
    int endElement = corotationalLinearFEMMT->GetEndElement(rank);

    //printf("%d %d\n", startElement, endElement);
    corotationalLinearFEMMT->ComputeForceAndStiffnessMatrixOfSubmesh(p, f, stiffnessMatrix, warp, startElement, endElement);

    //return NULL;
}

void CorotationalLinearFEMMT::ComputeForceAndStiffnessMatrix(double * p, double * f, SparseMatrix * stiffnessMatrix, int warp)
{
    // launch threads
    int numVertices3 = 3 * tetMesh->getNumVertices();
    
    for(int i=0; i<numThreads; i++)
    {
       threadArgv[i].corotationalLinearFEMMT = this;
       threadArgv[i].p = p;
       threadArgv[i].warp = warp;
    }

    //static PerformanceCounter cnt, cnt2;
    //cnt.StartCounter();
    //int * tid = (int*) malloc (sizeof(int) * numThreads);
    for(int i=0; i<numThreads; i++)  
    {
        SIMCON::CThreadPool::Instance().RunTask(CorotationalLinearFEMMT_WorkerThread, &threadArgv[i]);
      //if (pthread_create(&tid[i], NULL, CorotationalLinearFEMMT_WorkerThread, &threadArgv[i]) != 0)
      //{
      //  printf("Error: unable to launch thread %d.\n", i);
      //  exit(1);
      //}
    }

    //for(int i=0; i<numThreads; i++)
    {
        //SIMCON::CThreadPool::Instance().WaitForTask(tid[i], INFINITE);
        //SIMCON::CThreadPool::Instance().WaitForTasks(0, tid, true, INFINITE);
        SIMCON::CThreadPool::Instance().WaitForTasks(0, NULL, true, INFINITE);
      //if (pthread_join(tid[i], NULL) != 0)
      //{
      //  printf("Error: unable to join thread %d.\n", i);
      //  exit(1);
      //}
    }

    //free(threadArgv);
    //free(tid);

    //cnt.StopCounter();
    //cnt2.StartCounter();

    if (f)
      memset(f, 0, sizeof(double) * numVertices3);
    if (stiffnessMatrix)
        stiffnessMatrix->ResetToZero();

    for(int i=0; i<numThreads; i++)
    {
        if (f)
        {
           double * source = &internalForceBuffer[i * numVertices3];
           for(int j=0; j<numVertices3; j++)
             f[j] += source[j];
        }

        if (stiffnessMatrix)
            *stiffnessMatrix += *(stiffnessMatrixBuffer[i]);
    }
    //cnt2.StopCounter();
    
    //printf("time1: %0.9f time2: %0.9f\n", cnt.GetElapsedTime(), cnt2.GetElapsedTime());
}

void CorotationalLinearFEMMT::UpdatePlasticStrain(double *vertexPositions)
{
    CorotationalLinearFEM::UpdatePlasticStrain(vertexPositions);
}

void CorotationalLinearFEMMT::UpdateFPlastic(double *vertexPositions)
{
    CorotationalLinearFEM::UpdateFPlastic(vertexPositions);
}

static void UpdatePlasticDisplacementThread(void * arg)
{
    CorotationalLinearFEMMT::CorotationalLinearFEMMT_threadArg * threadArgp 
        = (CorotationalLinearFEMMT::CorotationalLinearFEMMT_threadArg*) arg;

    CorotationalLinearFEMMT * corotationalLinearFEMMT = threadArgp->corotationalLinearFEMMT;
    double * p = threadArgp->p;
    int rank = threadArgp->rank;
    int startElement = corotationalLinearFEMMT->GetStartElement(rank);
    int endElement = corotationalLinearFEMMT->GetEndElement(rank);

    //printf("%d %d\n", startElement, endElement);
    corotationalLinearFEMMT->UpdatePlasticDisplacementOfSubmesh(p, startElement, endElement);
}

void CorotationalLinearFEMMT::UpdatePlasticDisplacement(double *vertexPositions)
{
    // launch threads
    int numVertices3 = 3 * tetMesh->getNumVertices();
    
    for(int i=0; i < numThreads; i++)
    {
        threadArgv[i].corotationalLinearFEMMT = this;
        threadArgv[i].p = vertexPositions;
        SIMCON::CThreadPool::Instance().RunTask(UpdatePlasticDisplacementThread, &threadArgv[i]);
    }
    SIMCON::CThreadPool::Instance().WaitForTasks(0, NULL, true, INFINITE);
    //CorotationalLinearFEM::UpdatePlasticDisplacement(vertexPositions);
}

void CorotationalLinearFEMMT::UpdatePlasticMaterialForce(double *vertexPositions)
{
    CorotationalLinearFEM::UpdatePlasticMaterialForce(vertexPositions);
}

int CorotationalLinearFEMMT::GetStartElement(int rank)
{
  return startElement[rank];
}

int CorotationalLinearFEMMT::GetEndElement(int rank)
{
  return endElement[rank];
}

