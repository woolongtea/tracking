#pragma once

#include "FemComputer.h"

class FemCUDA : public FemComputer
{
public:
	FemCUDA(int numVert, int numElem);
	~FemCUDA(void);

	void mallocDevBuffers();
	void freeDevBuffers();
	void initDevBuffers(double ** MInverse, 
						double *undeformedPositions, 
						double ** KElementUndeformed,
						int *vertIndices);

	void launch(double * u, double * f, int warp, int elementLo, int elementHi);

protected:
	void updateDevBuffers(double * u);
	void readbackDevBuffers(double * f);

protected:
	double *dev_safeForce;
};
