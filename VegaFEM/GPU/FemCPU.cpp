#include "FemCPU.h"
#include <string.h>

#include "ComputeForce.h"

FemCPU::FemCPU(int numVert, int numElem)
: FemComputer(numVert, numElem)
{
}

FemCPU::~FemCPU(void)
{
}

void FemCPU::readbackDevBuffers(double * f)
{
	memcpy(f, _kenel_f, sizeof(double) * 3 * _numVertices);

	memcpy(_host_KElement, _kenel_KElement, sizeof(double) * _numElements * 144);
}

void FemCPU::updateDevBuffers(double * u)
{
	memset(_kenel_f, 0, sizeof(double) * 3 * _numVertices);

	memcpy(_kenel_u, u, sizeof(double) * 3 * _numVertices);
}

void FemCPU::initDevBuffers(double ** MInverse,
							 double *undeformedPositions, 
							 double ** KElementUndeformed,
							 int *vertIndices)
{
	memcpy(_kenel_vertIndex, vertIndices, sizeof(int) * 4 * _numElements);

	memcpy(_kenel_undeformedPositions, undeformedPositions, sizeof(double)*3 * _numVertices);

	for(int i=0; i<_numElements; i++)
		memcpy(&_kenel_MInverse[i*16], MInverse[i], sizeof(double)*16);

	for(int i=0; i<_numElements; i++)
		memcpy(&_kenel_KElementUndeformed[i*144], KElementUndeformed[i], sizeof(double)*144);
}

void FemCPU::mallocDevBuffers()
{
	_kenel_f = new double [3 * _numVertices];
	_kenel_u = new double [3 * _numVertices];
	_kenel_undeformedPositions = new double [3 * _numVertices];
	_kenel_MInverse = new double[_numElements * 16];
	_kenel_KElementUndeformed = new double[_numElements * 144];
	_kenel_KElement = new double[_numElements * 144];

	_kenel_vertIndex = new int[_numElements * 4];

	_host_KElement = new double[_numElements * 144];
}

void FemCPU::freeDevBuffers()
{
	delete [] _kenel_f;
	delete [] _kenel_KElement;

	delete [] _kenel_u;
	delete [] _kenel_undeformedPositions;
	delete [] _kenel_MInverse;
	delete [] _kenel_KElementUndeformed;

	delete [] _kenel_vertIndex;

	delete [] _host_KElement;
}

void FemCPU::launch(double * u, double * f, int warp, int elementLo, int elementHi)
{
	updateDevBuffers(u);
	for (int el=elementLo; el < elementHi; el++)
	{
		computeElementForce(	 _kenel_vertIndex,
								 _kenel_u, 
								 _kenel_f, 
								 warp,
								 elementLo,
								 elementHi,
								 el,
								 _kenel_undeformedPositions,
								 _kenel_MInverse,
								 _kenel_KElementUndeformed,
								 _kenel_KElement);
	}
	readbackDevBuffers(f);
}

void FemCPU::computeElementForce(	const int *vertIndex,
									const double * u, 
									double * f, 
									int warp,
									int elementLo,
									int elementHi,
									int el,
									const double * undeformedPositions,
									const double * MInverse,
									const double * KElementUndeformed,
									double * _kenel_KElement)
{
	int vtxIndex[4];
	for (int vtx=0; vtx<4; vtx++)
		vtxIndex[vtx] = vertIndex[el*4 + vtx];

	//double KElement[144]; // element stiffness matrix, to be computed below; row-major
	double *KElement = &_kenel_KElement[el*144];

	if (warp > 0)
	{
		double P[16]; // the current world-coordinate positions (row-major)
		double F[9]; // upper-left 3x3 block
		cudaPreComputePD(P, F, el, u, MInverse, undeformedPositions, vtxIndex);

		double R[9]; // rotation (row-major)
		//double S[9]; // symmetric (row-major)
		double det = cudaComputePolarDecomposition(F, R, 1E-6);
		if (det < 0)
		{
			// flip R so that it becomes orthogonal
			for(int i=0; i<9; i++)
				R[i] *= -1.0;
		}

		double RK[144]; // row-major
		cudaWarpMatrix(KElementUndeformed + el*144, R, RK, KElement);

		// f = RK (RT x - x0)
		cudaWarpForce(f, KElement, P, RK, undeformedPositions, vtxIndex);

	}
	else
	{
		// no warp
		memcpy(KElement, KElementUndeformed + el*144, sizeof(double) * 144);

		double fElement[12];
		cudaNoWarpForce(f, fElement, KElement, u, vtxIndex);
	}
}
