#include "FemCUDA.h"

#include "cuda_runtime_api.h"
#include "cutil.h"
#include "cutil_inline_runtime.h"

#include "ComputeForce.h"
#include "..\performanceCounter\performanceCounter.h"

FemCUDA::FemCUDA(int numVert, int numElem)
: FemComputer(numVert, numElem)
{
}

FemCUDA::~FemCUDA(void)
{
}

void FemCUDA::readbackDevBuffers(double * f)
{
	cutilSafeCall(cudaMemcpy(f, _kenel_f, sizeof(double) * 3 * _numVertices, cudaMemcpyDeviceToHost));

	//cutilSafeCall(cudaMemcpy(_host_KElement, _kenel_KElement, sizeof(double) * _numElements * 144, cudaMemcpyDeviceToHost));
}

void FemCUDA::updateDevBuffers(double * u)
{
	cutilSafeCall(cudaMemset(_kenel_f, 0, sizeof(double) * 3 * _numVertices));
	cutilSafeCall(cudaMemset(dev_safeForce, 0, sizeof(double) * 3 * _numVertices * _numElements));

	cutilSafeCall(cudaMemcpy(_kenel_u, u, sizeof(double) * 3 * _numVertices, cudaMemcpyHostToDevice));
}

void FemCUDA::initDevBuffers(double ** MInverse,
							 double *undeformedPositions, 
							 double ** KElementUndeformed,
							 int *vertIndices)
{
	cutilSafeCall(cudaMemcpy(_kenel_vertIndex, vertIndices, sizeof(int)*_numElements*4, cudaMemcpyHostToDevice));

	cutilSafeCall(cudaMemcpy(_kenel_undeformedPositions, undeformedPositions, sizeof(double)*3 * _numVertices, cudaMemcpyHostToDevice));


	double *tmpMInverse = new double [_numElements * 16];
	for(int i=0; i<_numElements; i++)
		memcpy(&tmpMInverse[i*16], MInverse[i], sizeof(double)*16);
	cutilSafeCall(cudaMemcpy(_kenel_MInverse, tmpMInverse, sizeof(double) * _numElements * 16, cudaMemcpyHostToDevice));

	double *tmpKElementUndeformed = new double [_numElements * 144];
	for(int i=0; i<_numElements; i++)
		memcpy(&tmpKElementUndeformed[i*144], KElementUndeformed[i], sizeof(double)*144);
	cutilSafeCall(cudaMemcpy(_kenel_KElementUndeformed, tmpKElementUndeformed, sizeof(double)*_numElements * 144, cudaMemcpyHostToDevice));

	//delete [] vertIndices;
	delete [] tmpMInverse;
	delete [] tmpKElementUndeformed;
}

void FemCUDA::mallocDevBuffers()
{
	cutilSafeCall(cudaMalloc((void**)&dev_safeForce, sizeof(double) * 3 * _numVertices * _numElements));

	cutilSafeCall(cudaMalloc((void**)&_kenel_f, sizeof(double) * 3 * _numVertices));
	cutilSafeCall(cudaMalloc((void**)&_kenel_u, sizeof(double) * 3 * _numVertices));
	cutilSafeCall(cudaMalloc((void**)&_kenel_undeformedPositions, sizeof(double) * 3 * _numVertices));
	cutilSafeCall(cudaMalloc((void**)&_kenel_MInverse, sizeof(double) * _numElements * 16));
	cutilSafeCall(cudaMalloc((void**)&_kenel_KElementUndeformed, sizeof(double) * _numElements * 144));
	cutilSafeCall(cudaMalloc((void**)&_kenel_KElement, sizeof(double) * _numElements * 144));

	cutilSafeCall(cudaMalloc((void**)&_kenel_vertIndex, sizeof(int) * _numElements * 4));

	_host_KElement = new double[_numElements * 144];
}

void FemCUDA::freeDevBuffers()
{
	cutilSafeCall(cudaFree(_kenel_f));
	cutilSafeCall(cudaFree(dev_safeForce));
	cutilSafeCall(cudaFree(_kenel_KElement));

	cutilSafeCall(cudaFree(_kenel_u));
	cutilSafeCall(cudaFree(_kenel_undeformedPositions));
	cutilSafeCall(cudaFree(_kenel_MInverse));
	cutilSafeCall(cudaFree(_kenel_KElementUndeformed));

	cutilSafeCall(cudaFree(_kenel_vertIndex));

	delete [] _host_KElement;
}

void FemCUDA::launch(double * u, 
					 double * f,
					 int warp,
					 int elementLo, 
					 int elementHi)
{
	updateDevBuffers(u);

	launchCudaComputeFeatureFEM(_kenel_vertIndex,
							_kenel_u, 
							_kenel_f, 
							dev_safeForce,
							_numVertices,
							warp,
							elementLo,
							elementHi,
							0,
							_kenel_undeformedPositions,
							_kenel_MInverse,
							_kenel_KElementUndeformed,
							_kenel_KElement);
    
	readbackDevBuffers(f);
}