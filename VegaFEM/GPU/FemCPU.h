#pragma once

#include "FemComputer.h"

class FemCPU : public FemComputer
{
public:
	FemCPU(int numVert, int numElem);
	~FemCPU(void);

	void mallocDevBuffers();
	void freeDevBuffers();
	void initDevBuffers(double ** MInverse, 
						double *undeformedPositions, 
						double ** KElementUndeformed,
						int *vertIndices);

	void launch(double * u, double * f, int warp, int elementLo, int elementHi);

protected:
	void updateDevBuffers(double * u);
	void readbackDevBuffers(double * f);
	void computeElementForce(const int *vertIndex,
									const double * u, 
									double * f, 
									int warp,
									int elementLo,
									int elementHi,
									int el,
									const double * undeformedPositions,
									const double * MInverse,
									const double * KElementUndeformed,
									double * dev_KElement);
};
