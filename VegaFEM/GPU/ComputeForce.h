#ifndef CUDA_COMPUTE_FORCE_H_
#define CUDA_COMPUTE_FORCE_H_

#include "cuda_runtime_api.h"
#include "cutil.h"
#include "cutil_inline_runtime.h"

extern __device__ __host__ void cudaPreComputePD( double P[16],
												  double F[9],		// upper-left 3x3 block
												  int	 el,
												  const double * u,
												  const double * MInverse,
												  const double * undeformedPositions,
												  const int vtxIndex[4]);

extern __device__ __host__ void cudaWarpMatrix(const double * K, const double * R, double * RK, double * RKRT);

extern __device__ __host__ void cudaWarpForce(  double * f, 
												const double KElement[144],
												const double P[16],
												const double RK[144],
												const double * undeformedPositions,
												const int vtxIndex[4]);

extern __device__ __host__ void cudaNoWarpForce( double * f,
												 double fElement[12],
												 const double KElement[144],
												 const double * u,
												 const int vtxIndex[4]);

extern __device__ __host__ double cudaComputePolarDecomposition(const double * M, 
																double * Q, 
																double tolerance);

extern __device__ __host__ void cudaStiffnessMatrix( double **Entryies,
													 int * rowIndex,
													 int * columnIndex,
													 double KElement[144]);

extern "C" void launchCudaComputeFeatureFEM(const int *vertIndex,
									  const double * u, 
									  double * f, 
									  double * safeForce,
									  int numVertices,
									  int warp,
									  int elementLo,
									  int elementHi,
									  int el,
									  const double * undeformedPositions,
									  const double * MInverse,
									  const double * KElementUndeformed,
									  double * dev_KElement);

extern "C"  void launchTestKernel(double * f);

#endif

