#pragma once

class FemComputer
{
public:
	FemComputer(int numVert, int numElem);
	virtual ~FemComputer(void);

	virtual void mallocDevBuffers() = 0;
	virtual void freeDevBuffers() = 0;
	virtual void initDevBuffers(double ** MInverse, 
						double *undeformedPositions, 
						double ** KElementUndeformed,
						int *vertIndices) = 0;

	virtual void launch(double * u, double * f, int warp, int elementLo, int elementHi) = 0;

	double * getHostKElement(int el) { return &_host_KElement[el*144]; }

protected:
	virtual void updateDevBuffers(double * u) = 0;
	virtual void readbackDevBuffers(double * f) = 0;

	double *_kenel_f;
	double *_kenel_KElement;

	double *_host_KElement;

	double *_kenel_u;
	double *_kenel_undeformedPositions;
	double *_kenel_MInverse;
	double *_kenel_KElementUndeformed;

	int	   *_kenel_vertIndex;

	int		_numVertices;
	int		_numElements;

public:
};
