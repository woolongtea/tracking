#ifndef POLAR_DECOMPOSITION_CU
#define POLAR_DECOMPOSITION_CU

#include <cuda_runtime_api.h>

// printf() is only supported
// for devices of compute capability 2.0 and higher
#if defined(__CUDA_ARCH__) && (__CUDA_ARCH__ < 200)
#define printf(f, ...) ((void)(f, __VA_ARGS__),0)
#endif

// compute the one-norm of a 3x3 matrix (row-major)
__device__ __host__ double oneNorm(const double * A)
{
  double norm = 0.0;
  for (int i=0; i<3; i++) 
  {
    double columnAbsSum = fabs(A[i + 0]) + fabs(A[i + 3]) + fabs(A[i + 6]);
    if (columnAbsSum > norm) 
      norm = columnAbsSum;
  }
  return norm;
}

// compute the inf-norm of a 3x3 matrix (row-major)
__device__ __host__ double infNorm(const double * A)
{
  double norm = 0.0;
  for (int i=0; i<3; i++) 
  {
    double rowSum = fabs(A[3 * i + 0]) + fabs(A[3 * i + 1]) + fabs(A[3 * i + 2]);
    if (rowSum > norm) 
      norm = rowSum;
  }
  return norm;
}

// cross product: c = a x b
inline __device__ __host__ void crossProduct(const double * a, const double * b, double * c)
{
  c[0] = a[1] * b[2] - a[2] * b[1];
  c[1] = a[2] * b[0] - a[0] * b[2];
  c[2] = a[0] * b[1] - a[1] * b[0];
}

// Input: M (3x3 mtx)
// Output: Q (3x3 rotation mtx), S (3x3 symmetric mtx)
__device__ __host__ double cudaComputePolarDecomposition(const double * M, 
														 double * Q, 
														 double tolerance)
{
  double Mk[9];
  double Ek[9];
  double det, M_oneNorm, M_infNorm, E_oneNorm;

  // Mk = M^T
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
      Mk[3 * i + j] = M[3 * j + i];

  M_oneNorm = oneNorm(Mk); 
  M_infNorm = infNorm(Mk);

  do 
  {
    double MadjTk[9];
 
    // row 2 x row 3
    crossProduct(&(Mk[3]), &(Mk[6]), &(MadjTk[0])); 
    // row 3 x row 1
    crossProduct(&(Mk[6]), &(Mk[0]), &(MadjTk[3]));
    // row 1 x row 2
    crossProduct(&(Mk[0]), &(Mk[3]), &(MadjTk[6]));

    det = Mk[0] * MadjTk[0] + Mk[1] * MadjTk[1] + Mk[2] * MadjTk[2];
    if (det == 0.0) 
    {
      //printf("Warning (polarDecomposition) : zero determinant encountered.\n");
      break;
    }

    double MadjT_one = oneNorm(MadjTk); 
    double MadjT_inf = infNorm(MadjTk);

    double gamma = sqrt(sqrt((MadjT_one * MadjT_inf) / (M_oneNorm * M_infNorm)) / fabs(det));
    double g1 = gamma * 0.5;
    double g2 = 0.5 / (gamma * det);

    for(int i=0; i<9; i++)
    {
      Ek[i] = Mk[i];
      Mk[i] = g1 * Mk[i] + g2 * MadjTk[i];
      Ek[i] -= Mk[i];
    }

    E_oneNorm = oneNorm(Ek);
    M_oneNorm = oneNorm(Mk);  
    M_infNorm = infNorm(Mk);
  }
  while ( E_oneNorm > M_oneNorm * tolerance );

  // Q = Mk^T 
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
      Q[3*i+j] = Mk[3*j+i];

  return (det);
}

#endif
