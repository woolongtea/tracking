#ifndef COMPUTE_FORCE_CU
#define COMPUTE_FORCE_CU

#include <cuda_runtime_api.h>
#include "cutil.h"
#include "cutil_inline_runtime.h"

#include "./PolarDecomposition.cu"
#include "./cudaSVD.cu"

//#define BLOCKSIZE 256

__device__ __host__ void cudaPreComputePD(double P[16],
										  double F[9],		// upper-left 3x3 block
										  int	 el,
										  const double * u,
										  const double * MInverse,
										  const double * undeformedPositions,
										  const int vtxIndex[4])
{
      /*
         P = [ v0   v1   v2   v3 ]
             [  1    1    1    1 ]
      */
      // rows 1,2,3
      for(int i=0; i<3; i++)
        for(int j=0; j<4; j++)
          P[4 * i + j] = undeformedPositions[3 * vtxIndex[j] + i] + u[3 * vtxIndex[j] + i];
      // row 4
      for(int j=0; j<4; j++)
        P[12 + j] = 1;

      // F = P * Inverse(M)
      for(int i=0; i<3; i++) 
        for(int j=0; j<3; j++) 
        {
          F[3 * i + j] = 0;
          for(int k=0; k<4; k++)
            F[3 * i + j] += P[4 * i + k] * MInverse[el*16 + 4 * k + j];
	}
}

__device__ __host__ void cudaWarpMatrix(const double * K, const double * R, double * RK, double * RKRT)
{
	memset(RK, 0, sizeof(double) * 144);
	memset(RKRT, 0, sizeof(double) * 144);
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
		{
			// RK = R * K
			for(int k=0; k<3; k++)
				for(int l=0; l<3; l++)
					for(int m=0; m<3; m++)
						RK[12 * (3 * i + k) + (3 * j + l)] += R[3 * k + m] * K[12 * (3 * i + m) + (3 * j + l)];

			// RKRT = RK * R^T
			for(int k=0; k<3; k++)
				for(int l=0; l<3; l++)
					for(int m=0; m<3; m++)
						RKRT[12 * (3 * i + k) + (3 * j + l)] += RK[12 * (3 * i + k) + (3 * j + m)] * R[3 * l + m];
		}
}

__device__ __host__ void cudaWarpForce( double * f, 
										const double KElement[144],
										const double P[16],
										const double RK[144],
										const double * undeformedPositions,
										const int vtxIndex[4])
{
	double fElement[12];
    // f = RK (RT x - x0)
	for(int i=0; i<12; i++)
	{
		fElement[i] = 0;
		for(int j=0; j<4; j++)
			for(int l=0; l<3; l++)
				fElement[i] += KElement[12 * i + 3 * j + l] * P[4 * l + j] - RK[12 * i + 3 * j + l] * undeformedPositions[3 * vtxIndex[j] + l];
	}

	// add fElement into the global f
	if (f != NULL)
	{
		for(int j=0; j<4; j++)
			for(int l=0; l<3; l++)
				f[3 * vtxIndex[j] + l] += fElement[3 * j + l];
	}
}

__device__ __host__ void cudaNoWarpForce(double * f,
										 double fElement[12],
										 const double KElement[144],
										 const double * u,
										 const int vtxIndex[4])
{
	for(int i=0; i<12; i++)
	{
		fElement[i] = 0;
		for(int j=0; j<4; j++)
		{
			fElement[i] += 
				KElement[12 * i + 3 * j + 0] * u[3 * vtxIndex[j] + 0] +
				KElement[12 * i + 3 * j + 1] * u[3 * vtxIndex[j] + 1] +
				KElement[12 * i + 3 * j + 2] * u[3 * vtxIndex[j] + 2];
		}
	}

	// add fElement into the global f
	if (f != NULL)
	{
		for(int j=0; j<4; j++)
		{
			f[3 * vtxIndex[j] + 0] += fElement[3 * j + 0];
			f[3 * vtxIndex[j] + 1] += fElement[3 * j + 1];
			f[3 * vtxIndex[j] + 2] += fElement[3 * j + 2];
		}
	}
}

__device__ __host__ void cudaStiffnessMatrix(double **Entryies,
											 int * rowIndex,
											 int * columnIndex,
											 double KElement[144])
{
      // add KElement to the global stiffness matrix
      for (int i=0; i<4; i++)
        for (int j=0; j<4; j++)
          for(int k=0; k<3; k++)
            for(int l=0; l<3; l++)
              Entryies[3 * rowIndex[i] + k][3 * columnIndex[4 * i + j] + l] += KElement[12 * (3 * i + k) + 3 * j + l];
}

__device__ void computePolarDecompositionWithSVD(double *R, const double *F)
{
	float sa[9], su[9], sv[9], sigma[3];
	for(int i=0; i<9; i++)
		sa[i] = F[i];
	computeSVD(sa, su, sv, sigma);
	R[0] = su[0] * sv[0] + su[1] * sv[1] + su[2] * sv[2];
	R[1] = su[0] * sv[3] + su[1] * sv[4] + su[2] * sv[5];
	R[2] = su[0] * sv[6] + su[1] * sv[7] + su[2] * sv[8];

	R[3] = su[3] * sv[0] + su[4] * sv[1] + su[5] * sv[2];
	R[4] = su[3] * sv[3] + su[4] * sv[4] + su[5] * sv[5];
	R[5] = su[3] * sv[6] + su[4] * sv[7] + su[5] * sv[8];

	R[6] = su[6] * sv[0] + su[7] * sv[1] + su[8] * sv[2];
	R[7] = su[6] * sv[3] + su[7] * sv[4] + su[8] * sv[5];
	R[8] = su[6] * sv[6] + su[7] * sv[7] + su[8] * sv[8];
}

__global__ void computeElementForce(const int *vertIndex,
									const double * u, 
									double * f, 
									double * safeForce,
									int numVertices,
									int warp,
									int elementLo,
									int elementHi,
									int el,
									const double * undeformedPositions,
									const double * MInverse,
									const double * KElementUndeformed,
									double * dev_KElement)
{
	el = elementLo + blockIdx.x * BLOCKSIZE + threadIdx.x;
	if ( el >= (elementHi-elementLo) )
		return;

	f = safeForce + el * 3 * numVertices;

	int vtxIndex[4];
	for (int vtx=0; vtx<4; vtx++)
		vtxIndex[vtx] = vertIndex[el*4 + vtx];

	//double KElement[144]; // element stiffness matrix, to be computed below; row-major
	double *KElement = &dev_KElement[el*144];

	if (warp > 0)
	{
		double P[16]; // the current world-coordinate positions (row-major)
		double F[9]; // upper-left 3x3 block
		cudaPreComputePD(P, F, el, u, MInverse, undeformedPositions, vtxIndex);

		double R[9]; // rotation (row-major)
		//double det = cudaComputePolarDecomposition(F, R, 1E-6);
		//if (det < 0)
		//{
		//	// flip R so that it becomes orthogonal
		//	for(int i=0; i<9; i++)
		//		R[i] *= -1.0;
		//}
		computePolarDecompositionWithSVD(R, F);

		// RK = R * K
		// KElement = R * K * R^T
		double RK[144]; // row-major
		cudaWarpMatrix(KElementUndeformed + el*144, R, RK, KElement);

		// f = RK (RT x - x0)
		cudaWarpForce(f, KElement, P, RK, undeformedPositions, vtxIndex);

	}
	else
	{
		// no warp
		memcpy(KElement, KElementUndeformed + el*144, sizeof(double) * 144);

		double fElement[12];
		cudaNoWarpForce(f, fElement, KElement, u, vtxIndex);
	}
}

__global__ void accumForce(	  double * f, 
							  double * safeForce,
							  int numVertices,
							  int elementLo,
							  int elementHi)
{
	int forceIdx = blockIdx.x * BLOCKSIZE + threadIdx.x;
	f[forceIdx] = 0.0;
	for(int i=elementLo; i<elementHi; i++)
		f[forceIdx] += safeForce[i*numVertices*3 + forceIdx];
}

extern "C" void launchCudaComputeFeatureFEM(const int *vertIndex,
							  const double * u, 
							  double * f, 
							  double * safeForce,
							  int numVertices,
							  int warp,
							  int elementLo,
							  int elementHi,
							  int el,
							  const double * undeformedPositions,
							  const double * MInverse,
							  const double * KElementUndeformed,
							  double * dev_KElement,
							  double * dev_TData)
{
	int blockSize = BLOCKSIZE;
	int gridSize = (elementHi-elementLo)/blockSize + ((elementHi-elementLo)%blockSize > 0);

	computeElementForce<<<gridSize, blockSize>>>(vertIndex,
													u, 
													f, 
													safeForce,
													numVertices,
													warp,
													elementLo,
													elementHi,
													el,
													undeformedPositions,
													MInverse,
													KElementUndeformed,
													dev_KElement);

	cutilSafeCall(cudaDeviceSynchronize());

	int fSize = numVertices*3;
	blockSize = BLOCKSIZE;
	gridSize = fSize/blockSize + (fSize%blockSize > 0);
	accumForce<<<gridSize, blockSize>>>(f, safeForce, numVertices, elementLo, elementHi);

	cutilSafeCall(cudaDeviceSynchronize());
}

__global__ void testKernel(double * f)
{
	f[threadIdx.x] = 2.5;
}
extern "C"  void launchTestKernel(double * f)
{
	testKernel<<<1, 10>>> (f);
}
#endif
