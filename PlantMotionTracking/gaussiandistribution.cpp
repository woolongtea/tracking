#include "gaussiandistribution.h"

GaussianDistribution::GaussianDistribution()
{
    Eigen::Matrix3d id = Eigen::Matrix3d::Identity();
    setCoefficient(id);
}

GaussianDistribution::~GaussianDistribution()
{

}

int GaussianDistribution::setCoefficient(Eigen::Matrix3d &A)
{
    m_covarianceMatrix = A;
    double det = m_covarianceMatrix.determinant();
    if (det < 1e-20)
        return -1;
    m_normalizedFactor = 1/sqrt(det);
    m_normalizedFactor /= pow(2 * M_PI, 3./2.);
    m_inverseCovMat = m_covarianceMatrix.inverse();
    return 0;
}

int GaussianDistribution::setCoefficient(const double covariance)
{
    Eigen::Matrix3d id = Eigen::Matrix3d::Identity();
    id *= covariance;
    setCoefficient(id);
    return 0;
}

int GaussianDistribution::setCoefficient(const double *A)
{
    Eigen::Matrix3d tmp;
    for (int i=0; i < 9; ++i)
        tmp(i) = A[i];
    return setCoefficient(tmp);
}

double GaussianDistribution::ComputeGaussProbability(const double *x)
{
    double P = 0;
    Eigen::Vector3d tx(x);
    Eigen::Vector3d v = m_inverseCovMat * tx;
    P = v.dot(tx);
    P /= -2.;
    P = exp(P) * m_normalizedFactor;
    return P;
}

double GaussianDistribution::ComputeGaussProbability(const Eigen::Vector3d &x)
{
    Eigen::Vector3d v = m_inverseCovMat * x;
    double P = v.dot(x);
    P /= -2.;
    P = exp(P) * m_normalizedFactor;
    return P;
}

double GaussianDistribution::operator()(const Eigen::Vector3d &x)
{
    return ComputeGaussProbability(x);
}
