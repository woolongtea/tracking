#include "FineMesh.h"
#include "glviewer.h"


FineMesh::FineMesh(GLViewer *glWidget)
    :m_glWidget(glWidget)
{
    m_mesh = std::make_shared<TriMeshType>();
}

FineMesh::~FineMesh(void)
{
}

void FineMesh::LoadFineMeshFromFile(const std::string &filename )
{
    m_mesh->request_vertex_normals();
    m_mesh->request_vertex_texcoords2D();
    m_opt += OpenMesh::IO::Options::VertexNormal;
    m_opt += OpenMesh::IO::Options::VertexTexCoord;
    if (! OpenMesh::IO::read_mesh(*m_mesh,filename,m_opt))
    {
        std::cerr << "Error: Cannot read mesh from " << std::endl;
        return;
    }

    if (!m_opt.check( OpenMesh::IO::Options::VertexTexCoord))
    {
        std::cout <<"Failed to load vertex texture coordinate" <<std::endl;
    }
    
    if ( !m_opt.check( OpenMesh::IO::Options::VertexNormal ) )
    {
        // we need face normals to update the vertex normals
        m_mesh->request_face_normals();
        // let the mesh update the normals
        m_mesh->update_normals();
        //m_mesh->release_face_normals();
    }
    m_visibility.resize(m_mesh->n_vertices());
    m_interpWeights.resize(m_mesh->n_vertices());
    m_interpIndices.resize(m_mesh->n_vertices());
    m_trackFineForces.resize(m_mesh->n_vertices());
    for (size_t i=0;i<m_mesh->n_vertices();++i)
    {
        m_interpIndices[i].resize(4);
    }
}

void FineMesh::SetVertices( std::vector<Vector3d>& v )
{
    if (m_mesh->n_vertices() != v.size())
    {
        std::cerr << "The number of vertices is not equal."<<std::endl;
        return;
    }
    
    for (size_t i=0; i< v.size(); ++i)
    {
        m_mesh->point(m_mesh->vertex_handle(i))[0] = v[i][0];
        m_mesh->point(m_mesh->vertex_handle(i))[1] = v[i][1];
        m_mesh->point(m_mesh->vertex_handle(i))[2] = v[i][2];
    }

    m_mesh->update_normals();
}

void FineMesh::DrawFineMesh()
{
    const qglviewer::Vec cameraPos = m_glWidget->camera()->position();
    const GLfloat pos[4] = {cameraPos[0], cameraPos[1], cameraPos[2], 1.0};
    glLightfv(GL_LIGHT1, GL_POSITION, pos);
    if (m_mesh->n_vertices() != 0)
    {
        glColor3f(0.2,0.8,0.2);
        glBegin(GL_TRIANGLES);
        for (auto fit= m_mesh->faces_begin();fit != m_mesh->faces_end();++fit)
        {			
            for (auto fvit= m_mesh->fv_iter(fit.handle());fvit.is_valid();++fvit)
            {
                glNormal3fv(m_mesh->normal(fvit.handle()).data());
                glVertex3fv(m_mesh->point(fvit.handle()).data());
            }
        }
        glEnd();
    }
}

void FineMesh::DrawVisiblePoints()
{
    glDisable(GL_LIGHTING);
    glColor3f(1,1,0);
    glPointSize(3.0f);
    glBegin(GL_POINTS);
    for (size_t i=0;i<m_visibility.size();++i)
    {
        if (m_visibility[i] !=0)
            glVertex3fv(m_mesh->point(m_mesh->vertex_handle(i)).data());
    }
    glEnd();
    glEnable(GL_LIGHTING);
}

void FineMesh::UpdateVerticesByInterpolating( std::vector<Vector3d>& tetVertices )
{
    size_t n_v = m_mesh->n_vertices();
    for (size_t i=0; i<n_v; ++i)
    {
        Vector3d v = tetVertices[m_interpIndices[i][0]] * m_interpWeights[i][0]
                   + tetVertices[m_interpIndices[i][1]] * m_interpWeights[i][1]
                   + tetVertices[m_interpIndices[i][2]] * m_interpWeights[i][2]
                   + tetVertices[m_interpIndices[i][3]] * m_interpWeights[i][3];
        m_mesh->point(m_mesh->vertex_handle(i))[0] = v[0];
        m_mesh->point(m_mesh->vertex_handle(i))[1] = v[1];
        m_mesh->point(m_mesh->vertex_handle(i))[2] = v[2];
    }
    m_mesh->update_normals();
}

void FineMesh::LoadInterpMatFromFile(const QString &filename )
{
    QFile interpFile(filename);
    if (!interpFile.open(QIODevice::ReadOnly))
    {       
        std::cerr<<"Fail to open interpolation file"<<std::endl;
        return;
    }

    QTextStream fin(&interpFile);
    size_t id = 0;
    while (!fin.atEnd())
    {
        QString line = fin.readLine();
        line = line.simplified();
        if (line.isEmpty() || line[0].isSpace())
            continue;
        QStringList segs = line.split(' ');
        m_interpIndices[id][0]=segs[1].toInt();
        m_interpIndices[id][1]=segs[3].toInt();
        m_interpIndices[id][2]=segs[5].toInt();
        m_interpIndices[id][3]=segs[7].toInt();
        m_interpWeights[id][0]=segs[2].toDouble();
        m_interpWeights[id][1]=segs[4].toDouble();
        m_interpWeights[id][2]=segs[6].toDouble();
        m_interpWeights[id][3]=segs[8].toDouble();
        ++id;
    }
}

std::vector<double> FineMesh::GetVerticesBuffer()
{
    std::vector<double> vb;
    for (auto vi=m_mesh->vertices_begin(); vi != m_mesh->vertices_end(); ++ vi)
    {
        vb.push_back(m_mesh->point(vi.handle())[0]);
        vb.push_back(m_mesh->point(vi.handle())[1]);
        vb.push_back(m_mesh->point(vi.handle())[2]);
    }
    return vb;
}

std::vector<int> FineMesh::GetFaceVertexIndices()
{
    std::vector<int> idx;
    for (auto fit=m_mesh->faces_begin();fit!=m_mesh->faces_end();++fit)
    {
        for (auto fvit=m_mesh->fv_iter(fit.handle()); fvit.is_valid(); ++fvit)
        {
            idx.push_back(fvit->idx());
        }
    }
    return idx;
}

void FineMesh::MapFineForces2CoarseForces( std::vector<Vector3d>& coarseForces )
{
    std::fill(coarseForces.begin(),coarseForces.end(),Vector3d::ZERO);
    for (size_t i=0; i<m_interpWeights.size(); ++i)
    {
        for (size_t j=0; j<4; ++j)
        {
            coarseForces[m_interpIndices[i][j]] += m_trackFineForces[i] * m_interpWeights[i][j];
        }
    }
}

void FineMesh::DumpCurrentMesh(const std::string & filename)
{
    if (!OpenMesh::IO::write_mesh(*m_mesh,filename,m_opt))
    {
        std::cerr<<"Failed to save mesh."<<std::endl;
    }
}
