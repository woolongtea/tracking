#pragma once

#include "comHeader.h"
#include <QFile>
#include <QString>
#include <QtOpenGL>
#include <QMessageBox>
#include <QTextStream>
#include <vector>
#include <memory>
#include <string>


class GLViewer;
class FineMesh
{
public:
    FineMesh(GLViewer *glWidget);
    ~FineMesh(void);
    void LoadFineMeshFromFile(const std::string &filename);
    void LoadInterpMatFromFile(const QString &filename);
    void SetVertices(std::vector<Vector3d>& v);
    void UpdateVerticesByInterpolating(std::vector<Vector3d>& tetVertices);
    void DrawFineMesh();
    void DrawVisiblePoints();
    void MapFineForces2CoarseForces(std::vector<Vector3d>& coarseForces);
    void DumpCurrentMesh(const std::string & filename);

    std::vector<double> GetVerticesBuffer();
    std::vector<int>    GetFaceVertexIndices();
    std::vector<Vector3d>& GetTrackFineForces() {return m_trackFineForces;}
    std::vector<int>&   GetVisibility()  { return m_visibility;}
    int  GetVerticesNum() { return m_mesh->n_vertices();}
    int  GetFacesNum()    { return m_mesh->n_faces();}
    std::shared_ptr<TriMeshType> GetOpenMesh() {return m_mesh;}

private:
    //m_fineMeshVertices store the tracking result of all frames
    //[0] store the reset_shape
    std::vector< std::vector<Vector3d> > m_fineMeshVertices;
    std::shared_ptr<TriMeshType> m_mesh;
    OpenMesh::IO::Options m_opt;
    GLViewer *m_glWidget;
    std::vector<int> m_visibility;
    std::vector<Vector4d> m_interpWeights;
    std::vector<std::vector<int>>  m_interpIndices;
    std::vector<Vector3d> m_trackFineForces;
};

