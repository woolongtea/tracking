#include "glviewer.h"

GLViewer::GLViewer(QWidget *parent)
    :m_flagTetFrame(true)
    ,m_flagPC(true)
    ,m_glTetMesh(nullptr)
    ,m_glFineMesh(nullptr)
{
}

GLViewer::~GLViewer(void)
{
}

void GLViewer::init()
{
    restoreStateFromFile();
    setSceneRadius(2.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    setBackgroundColor(QColor(0,0,0));
    setKeyDescription(Qt::Key_W, "Toggles wire frame display");

    glEnable(GL_LIGHTING);
    glDisable(GL_POINT_SMOOTH);
    ////glEnableClientState(GL_VERTEX_ARRAY);
    ////glEnableClientState(GL_NORMAL_ARRAY);
    ////glEnableClientState(GL_COLOR_ARRAY);
    // Light setup
    glDisable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    //Light default parameters
    const GLfloat light_ambient[4]  = {0.2f, 0.2f,0.2f, 1.0};
    const GLfloat light_diffuse[4]  = {0.8f, 0.8f, 0.8f, 1.0};
    const GLfloat light_specular[4] = {0.0, 0.0, 0.0, 1.0};
    ////glLightf( GL_LIGHT1, GL_SPOT_EXPONENT, 3.0);
    ////glLightf( GL_LIGHT1, GL_SPOT_CUTOFF,   10.0);
    glLightf( GL_LIGHT1, GL_CONSTANT_ATTENUATION,  0.1f);
    glLightf( GL_LIGHT1, GL_LINEAR_ATTENUATION,    0.3f);
    glLightf( GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.3f);
    glLightfv(GL_LIGHT1, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT1, GL_DIFFUSE,  light_diffuse);
}

void GLViewer::draw()
{
      //DrawTestBox();
      if (m_flagTetFrame)
      {
        DrawTetMesh();
      }
      DrawFineMesh();
      if (m_flagPC)
      {
        DrawPointCloud();
      }
}

void GLViewer::DrawCornerAxis()
{
    int viewport[4];
    int scissor[4];

    // The viewport and the scissor are changed to fit the lower left
    // corner. Original values are saved.
    glGetIntegerv(GL_VIEWPORT, viewport);
    glGetIntegerv(GL_SCISSOR_BOX, scissor);

    // Axis viewport size, in pixels
    const int size = 150;
    glViewport(0,0,size,size);
    glScissor(0,0,size,size);

    // The Z-buffer is cleared to make the axis appear over the
    // original image.
    glClear(GL_DEPTH_BUFFER_BIT);

    // Tune for best line rendering
    glDisable(GL_LIGHTING);
    glLineWidth(3.0);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(-1, 1, -1, 1, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glMultMatrixd(camera()->orientation().inverse().matrix());

    glBegin(GL_LINES);
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(1.0, 0.0, 0.0);

    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 1.0, 0.0);

    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 1.0);
    glEnd();


    glEnable(GL_LIGHTING);
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    // The viewport and the scissor are restored.
    glScissor(scissor[0],scissor[1],scissor[2],scissor[3]);
    glViewport(viewport[0],viewport[1],viewport[2],viewport[3]);
}

void GLViewer::postDraw()
{
    QGLViewer::postDraw();
    DrawCornerAxis();
}

void GLViewer::keyPressEvent( QKeyEvent *e )
{
    //// Get event modifiers key
    const Qt::KeyboardModifiers modifiers = e->modifiers();

    //// A simple switch on e->key() is not sufficient if we want to take state key into account.
    //// With a switch, it would have been impossible to separate 'F' from 'CTRL+F'.
    //// That's why we use imbricated if...else and a "handled" boolean.
    bool handled = false;
    if ((e->key()==Qt::Key_W) && (modifiers==Qt::NoButton))
    {
        m_flagTetFrame = !m_flagTetFrame;
        handled = true;
        updateGL();
    }
    if ((e->key()==Qt::Key_P) && (modifiers==Qt::NoButton))
    {
        m_flagPC = !m_flagPC;
        handled = true;
        updateGL();
    }

    //// ... and so on with other else/if blocks.

    if (!handled)
        QGLViewer::keyPressEvent(e);
}

void GLViewer::DrawTestBox()
{

    // Place light at camera position
    const qglviewer::Vec cameraPos = camera()->position();
    //std::cout<<cameraPos[0]<<" "<<cameraPos[1]<<" "<<cameraPos[2]<<std::endl;
    const GLfloat pos[4] = {cameraPos[0], cameraPos[1], cameraPos[2], 1.0};
    glLightfv(GL_LIGHT1, GL_POSITION, pos);
    float pfVer[][3] = {-0.200000, -0.200000, -0.200000,
        0.200000, -0.200000, -0.200000,
        -0.200000, 0.200000, -0.200000,
        0.200000, 0.200000, -0.200000,
        -0.200000, -0.200000, 0.200000,
        0.200000, -0.200000, 0.200000,
        -0.200000, 0.200000, 0.200000,
        0.200000, 0.200000, 0.200000};

    float color[] = {1.0,1.0,0.0,
        1.0,1.0,0.0,
        1.0,1.0,0.0,
        1.0,1.0,0.0,
        1.0,1.0,0.0,
        1.0,1.0,0.0,
        1.0,1.0,0.0,
        1.0,1.0,0.0};

    unsigned int piInd[][3] = { 2,1,0,
        1,2,3,
        4,2,0,
        2,4,6,
        1,4,0,
        4,1,5,
        6,5,7,
        5,6,4,
        3,6,7,
        6,3,2,
        5,3,7,
        3,5,1};

    float pfNorm[][3] = {-0.577350, -0.577350, -0.577350,
        0.333333, -0.666667, -0.666667,
        -0.666667, 0.333333, -0.666667,
        0.666667, 0.666667, -0.333333,
        -0.666667, -0.666667, 0.333333,
        0.666667, -0.333333, 0.666667,
        -0.333333, 0.666667, 0.666667,
        0.577350, 0.577350, 0.577350};


    //glVertexPointer(3,GL_FLOAT, 0, pfVer);
    //glNormalPointer(GL_FLOAT,0,pfNorm);
    //glColorPointer(3,GL_FLOAT,0,color);
    //glDrawElements(GL_TRIANGLES,12*3,GL_UNSIGNED_INT,piInd);

    glColor3f(1,1,0);
    glBegin(GL_TRIANGLES);
    for (int i=0;i<12;++i)
    {
        glNormal3fv(pfNorm[piInd[i][0]]);
        glVertex3fv(pfVer[piInd[i][0]]);
        glNormal3fv(pfNorm[piInd[i][1]]);
        glVertex3fv(pfVer[piInd[i][1]]);
        glNormal3fv(pfNorm[piInd[i][2]]);
        glVertex3fv(pfVer[piInd[i][2]]);
    }
    glEnd();
}

void GLViewer::DrawMesh()
{   
    //if (m_glMesh != nullptr)
    //{
    //    const qglviewer::Vec cameraPos = camera()->position();
    //    //std::cout<<cameraPos[0]<<" "<<cameraPos[1]<<" "<<cameraPos[2]<<std::endl;
    //    const GLfloat pos[4] = {cameraPos[0], cameraPos[1], cameraPos[2], 1.0};
    //    glLightfv(GL_LIGHT1, GL_POSITION, pos);

    //    glColor3f(0.8,0.8,0.2);
    //    //DrawTestBox();
    //    glBegin(GL_TRIANGLES);
    //    for (auto fit= m_glMesh->faces_begin();fit != m_glMesh->faces_end();++fit)
    //    {			
    //        for (auto fvit= m_glMesh->fv_iter(fit.handle());fvit.is_valid();++fvit)
    //        {
    //        glNormal3fv(m_glMesh->normal(fvit.handle()).data());
    //        glVertex3fv(m_glMesh->point(fvit.handle()).data());
    //        }
    //    }
    //    glEnd();
    //}

    //glColor3f(0,1,0);
    //glPointSize(5.0f);
    ////test_v[5] =1;
    //glBegin(GL_POINTS);
    //for (int i=0;i<test_v.size();++i)
    //{
    //    if (test_v[i] !=0)
    //    glVertex3fv(m_glMesh->point(m_glMesh->vertex_handle(i)).data());
    //}
    //glEnd();
}

void GLViewer::SetupProjViewModelMatrixForShader( QGLShaderProgram &shader )
{
    QMatrix4x4 projViewModelMat;
    QMatrix4x4 viewMat;
    QMatrix4x4 projMat;
    double proj[16];
    double modelview[16];
    camera()->getProjectionMatrix(proj);
    camera()->getModelViewMatrix(modelview);

    for (int i=0;i<4;++i)
    {
        for (int j=0;j<4;++j)
        {
            projMat(j,i) = proj[4*i+j];
            viewMat(j,i) = modelview[4*i+j];
        }
    }

    projViewModelMat = projMat * viewMat;

    shader.setUniformValue("uProjViewModelmatrix", projViewModelMat);
    shader.setUniformValue("uProjMatrix"         , projMat);
    shader.setUniformValue("uViewMatrix"         , viewMat);
    shader.setUniformValue("uNormalMatrix"       , projViewModelMat.normalMatrix());
}

void GLViewer::SetupLightsForShader( QGLShaderProgram &shader )
{
    const qglviewer::Vec cameraPos = camera()->position();
    shader.setUniformValue("uLightPos"                , QVector4D(cameraPos[0],
                                                                  cameraPos[1],
                                                                  cameraPos[2],
                                                                  1.0f));
    shader.setUniformValue("uLightMaterial.ambient"   , QVector4D(0.35f, 0.35f, 0.35f, 1.0f));
    shader.setUniformValue("uLightMaterial.diffuse"   , QVector4D(0.5f, 0.5f, 0.5f, 1.0f));
    shader.setUniformValue("uLightMaterial.specular"  , QVector4D(0.8f, 0.8f, 0.8f, 1.0f));
}

void GLViewer::ClearTetMesh()
{
    //clear
    m_glTetMesh = nullptr;
}

void GLViewer::DrawTetMesh()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    if (m_glTetMesh != nullptr)
    {
        m_glTetMesh->UpdateVertexNormal();
        m_glTetMesh->DrawTetMesh();
    }
}

void GLViewer::DrawFineMesh()
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    if (m_glFineMesh!=nullptr)
    {
        m_glFineMesh->DrawFineMesh();
        m_glFineMesh->DrawVisiblePoints();
    }
}

void GLViewer::DrawPointCloud()
{
    if (m_glPointCloud.size() != 0)
    {
        glColor3f(0.8,0.0,1);
        glPointSize(2.0f);
        glDisable(GL_LIGHTING);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3,GL_DOUBLE,0,&m_glPointCloud[0]);
        glDrawArrays(GL_POINTS,0,m_glPointCloud.size()/3);
        glEnable(GL_LIGHTING);
    }
}
