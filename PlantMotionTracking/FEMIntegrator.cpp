#include "FEMIntegrator.h"

#include "TetMesh.h"
#include "CorotatedLinearModel.h"
#include "VegaFEMInterface.h"

#include <functional>

namespace VCC
{

	FEMIntegrator::FEMIntegrator(CorotatedLinearModel *pModel, TetMesh *tetMesh)
		: m_pModel(pModel)
		, m_pTetMesh(tetMesh)
		, m_dampingStiffnessCoef(0.0)
		, m_dampingMassCoef(0.0)
		, m_gravity(Vector3d::ZERO)
		, m_integratorId(0)
		, m_flagNeedReInitInternalSolver(true)
	{
		m_nodalPosition = tetMesh->GetVertices();
		m_nodalVelocity.resize(m_nodalPosition.size(), Vector3d::ZERO);
		m_nodalForce.resize(m_nodalPosition.size(), Vector3d::ZERO);

		m_nodalPositionLast = tetMesh->GetVertices();
		m_nodalVelocityLast.resize(m_nodalPosition.size(), Vector3d::ZERO);

		m_vertMass = tetMesh->GetVertexMass();

		m_externalForce.resize(m_nodalPosition.size(), Vector3d::ZERO);
		m_gravityForce.resize(m_nodalPosition.size(), Vector3d::ZERO);
		m_airForce.resize(m_nodalPosition.size(), Vector3d::ZERO);
		m_zeroForce.resize(m_nodalPosition.size(), Vector3d::ZERO);

		//VegaFEMInterface::Instance(m_integratorId)->Initialize(
		//    pModel->GetTopology(),
		//    m_vertMass,
		//    std::vector<int>());
	}


	FEMIntegrator::~FEMIntegrator(void)
	{
	}

	void FEMIntegrator::ClearInternalForce(void)
	{
		std::fill(m_nodalForce.begin(), m_nodalForce.end(), Vector3d::ZERO);
	}

	void FEMIntegrator::ClearExternalForce(void)
	{
		std::fill(m_externalForce.begin(), m_externalForce.end(), Vector3d::ZERO);
	}

	void FEMIntegrator::AddExternalForce(const std::vector<Vector3d> &forces)
	{
		std::transform(forces.begin(), forces.end(), m_externalForce.begin(), m_externalForce.begin(), std::plus<Vector3d>());
	}


	void FEMIntegrator::SetGravity(const Vector3d &g)
	{
		m_gravity = g;
		for (size_t i = 0; i < m_vertMass.size(); ++i)
			m_gravityForce[i] = m_vertMass[i] * g;
	}

	void FEMIntegrator::SetDampingParameters(double dampingStiffnessCoef, double dampingMassCoef)
	{
		m_dampingStiffnessCoef = dampingStiffnessCoef;
		m_dampingMassCoef = dampingMassCoef;
	}

	void FEMIntegrator::ConstrainNodesInRange(double range[6], TetMesh *tetMesh)
	{
		std::tcout << TEXT("Checking constrained nodes ...") << std::endl;
		auto &vertices = tetMesh->GetVertices();
		m_constrainedDof.clear();
		for (size_t i = 0; i < vertices.size(); ++i)
		{
			auto &vt = vertices[i];
			if (vt[0] >= range[0] && vt[0] <= range[1] &&
				vt[1] >= range[2] && vt[1] <= range[3] &&
				vt[2] >= range[4] && vt[2] <= range[5])
			{
				m_constrainedDof.push_back(i * 3);
				m_constrainedDof.push_back(i * 3 + 1);
				m_constrainedDof.push_back(i * 3 + 2);
				std::tcout << i << TEXT(" ");
			}
		}
		std::tcout << std::endl;

		std::sort(m_constrainedDof.begin(), m_constrainedDof.end());
		m_constrainedDofPosBuf.resize(m_constrainedDof.size());
		m_constrainedDofVelBuf.resize(m_constrainedDof.size());
		m_flagNeedReInitInternalSolver = true;
	}

	void FEMIntegrator::ConstrainNodesByIndex(std::vector<int> nodeIdx)
	{
		std::tcout << TEXT("Constrained node: ") << std::endl;
		for (auto ni : nodeIdx)
		{
			m_constrainedDof.push_back(ni * 3);
			m_constrainedDof.push_back(ni * 3 + 1);
			m_constrainedDof.push_back(ni * 3 + 2);
			std::tcout << ni << TEXT(" ");
		}
		std::tcout << std::endl;

		std::sort(m_constrainedDof.begin(), m_constrainedDof.end());
		m_constrainedDofPosBuf.resize(m_constrainedDof.size());
		m_constrainedDofVelBuf.resize(m_constrainedDof.size());
		m_flagNeedReInitInternalSolver = true;
	}

	void FEMIntegrator::SetState(const std::vector<Vector3d> &nodalPosition, const std::vector<Vector3d> &nodalVelocity /*= std::vector<Vector3d>()*/)
	{
		if (m_nodalPosition.size() > nodalPosition.size())
		{
			std::tcout << TEXT("Warning! Insufficient positions are given!") << std::endl;
			std::copy(nodalPosition.begin(), nodalPosition.end(), m_nodalPosition.begin());
		}
		else
			std::copy(nodalPosition.begin(), nodalPosition.begin() + m_nodalPosition.size(), m_nodalPosition.begin());

		if (nodalVelocity.empty())
			std::fill(m_nodalVelocity.begin(), m_nodalVelocity.end(), Vector3d::ZERO);
		else if (m_nodalVelocity.size() > nodalVelocity.size())
		{
			std::tcout << TEXT("Warning! Insufficient velocities are given!") << std::endl;
			std::copy(nodalVelocity.begin(), nodalVelocity.end(), m_nodalVelocity.begin());
		}
		else
			std::copy(nodalVelocity.begin(), nodalVelocity.begin() + m_nodalVelocity.size(), m_nodalVelocity.begin());

		std::fill(m_nodalForce.begin(), m_nodalForce.end(), Vector3d::ZERO);

		m_nodalPositionLast = m_nodalPosition;
		m_nodalVelocityLast = m_nodalVelocity;
	}

	void FEMIntegrator::SymplecticEulerStep(double t)
	{
		m_nodalPositionLast = m_nodalPosition;
		m_nodalVelocityLast = m_nodalVelocity;

		// update elastic forces
		m_pModel->ResetAllFlags();
		m_pModel->ComputeElasticForce(m_nodalPosition, m_nodalForce, CorotatedLinearModel::StressTensor);

		// Ma + f_e = fext;
		size_t cdof = m_constrainedDof.size();

		double *pos = reinterpret_cast<double *>(m_nodalPosition.data());
		double *vel = reinterpret_cast<double *>(m_nodalVelocity.data());
		for (size_t ci = 0; ci < cdof; ++ci)
		{
			m_constrainedDofPosBuf[ci] = pos[m_constrainedDof[ci]];
			m_constrainedDofVelBuf[ci] = vel[m_constrainedDof[ci]];
		}

		auto &mass = m_vertMass;
		size_t nvert = m_nodalPosition.size();
		for (size_t vi = 0; vi < nvert; ++vi)
		{
			Vector3d a = m_nodalForce[vi];
			a = -a;
			a += m_externalForce[vi];
			a /= mass[vi];
			a += m_gravity;

			m_nodalVelocity[vi] += a * t;
			m_nodalPosition[vi] += m_nodalVelocity[vi] * t;
		}

		for (size_t ci = 0; ci < cdof; ++ci)
		{
			pos[m_constrainedDof[ci]] = m_constrainedDofPosBuf[ci];
			vel[m_constrainedDof[ci]] = m_constrainedDofVelBuf[ci];
		}
	}

	bool FEMIntegrator::BackwardEulerStep(double t)
	{
		m_nodalPositionLast = m_nodalPosition;
		m_nodalVelocityLast = m_nodalVelocity;

		size_t cdof = m_constrainedDof.size();

		double *pos = reinterpret_cast<double *>(m_nodalPosition.data());
		double *vel = reinterpret_cast<double *>(m_nodalVelocity.data());
		for (size_t ci = 0; ci < cdof; ++ci)
		{
			m_constrainedDofPosBuf[ci] = pos[m_constrainedDof[ci]];
			m_constrainedDofVelBuf[ci] = vel[m_constrainedDof[ci]];
		}


		if (m_flagNeedReInitInternalSolver)
		{
			VegaFEMInterface::Instance(m_integratorId)->Initialize(
				m_pModel->GetTopology(),
				m_vertMass,
				m_constrainedDof);
			m_flagNeedReInitInternalSolver = false;
		}

		m_pModel->ResetAllFlags();
		m_pModel->UpdateForceStiffnessMatrix(m_nodalPosition);
		m_pModel->ComputeElasticForce(m_nodalPosition, m_nodalForce, CorotatedLinearModel::StiffneeMatrix);
		auto &elmStiffness = m_pModel->GetElementStiffnessMatrix();

		for (size_t i = 0; i < m_addedForceVert.size(); ++i)
			m_externalForce[m_addedForceVert[i]] = m_addedForces[i];

		VegaFEMInterface::StepTask task = {            
			m_nodalPosition,        // std::vector<Vector3d> &nodalPosition;
			m_nodalVelocity,        // std::vector<Vector3d> &nodalVelocity;
			m_nodalForce,           // const std::vector<Vector3d> &nodalForce;
			m_externalForce,        // const std::vector<Vector3d> &externalForce;
			m_gravityForce,         // const Vector3d &g;
			elmStiffness,           // const std::vector<std::array<double, 12*12>> &elmStiffness;
			m_dampingStiffnessCoef, // double dampingStiffnessCoef;
			m_dampingMassCoef       // double dampingMassCoef;
		};
		bool flag = VegaFEMInterface::Instance(m_integratorId)->DoTimeStep(t, task);

		for (size_t ci = 0; ci < cdof; ++ci)
		{
			pos[m_constrainedDof[ci]] = m_constrainedDofPosBuf[ci];
			vel[m_constrainedDof[ci]] = m_constrainedDofVelBuf[ci];
		}

		return flag;
	}


	void FEMIntegrator::AddForce(const std::vector<int> &vert, const std::vector<Vector3d> &forces)
	{
		m_addedForceVert.insert(m_addedForceVert.end(), vert.begin(), vert.end());
		m_addedForces.insert(m_addedForces.end(), forces.begin(), forces.end());
	}

	void FEMIntegrator::AddAirDragForce(const std::vector<Vector3d> &forces)
	{
		std::fill(m_airForce.begin(), m_airForce.end(), Vector3d::ZERO);

		for(int i=0; i<m_pTetMesh->GetSurfaceFaces().size()/3; i++)
		{
			int ind1 = m_pTetMesh->GetSurfaceFaces()[i*3+0];
			int ind2 = m_pTetMesh->GetSurfaceFaces()[i*3+1];
			int ind3 = m_pTetMesh->GetSurfaceFaces()[i*3+2];

			Vector3d p1 = m_pTetMesh->GetVertices()[ind1];
			Vector3d p2 = m_pTetMesh->GetVertices()[ind2];
			Vector3d p3 = m_pTetMesh->GetVertices()[ind3];

			Vector3d face_velocity = 0.333*(m_nodalVelocityLast[ind1]+m_nodalVelocityLast[ind2]+m_nodalVelocityLast[ind3]);


			Vector3d face_normal = (p2-p1).UnitCross((p3-p2));
		}
	}

    void FEMIntegrator::BecomeQuasiStatic()
    {
        std::fill(m_nodalVelocity.begin(),m_nodalVelocity.end(),Vector3d::ZERO);
        ClearInternalForce();
    }

    const std::vector<Vector3d> FEMIntegrator::GetNodalDisplacement()
    {
        std::vector<Vector3d> nodal_displacement;

        for (size_t i=0; i<m_nodalPosition.size(); ++i)
        {
            nodal_displacement.push_back(m_nodalPosition[i] - m_nodalPositionLast[i]);
        }
        return nodal_displacement;
    }



}