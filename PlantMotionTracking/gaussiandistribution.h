#ifndef GAUSSIANDISTRIBUTION_H
#define GAUSSIANDISTRIBUTION_H

#include "comHeader.h"

class GaussianDistribution
{
public:
    GaussianDistribution();
    ~GaussianDistribution();
    double ComputeGaussProbability(const double *x);
    double ComputeGaussProbability(const Eigen::Vector3d& x);
    double operator ()(const Eigen::Vector3d &x);

    int setCoefficient(Eigen::Matrix3d &A);
    int setCoefficient(const double *A);
    int setCoefficient(const double covariance);

    const Eigen::Matrix3d &getInverseCovMat() { return m_inverseCovMat; }
    const Eigen::Matrix3d &getCovarianceMatrix() {return m_covarianceMatrix;}

private:
    Eigen::Matrix3d m_covarianceMatrix;
    Eigen::Matrix3d m_inverseCovMat;
    double m_normalizedFactor;
};

#endif // GAUSSIANDISTRIBUTION_H
