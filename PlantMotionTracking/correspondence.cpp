#include "correspondence.h"
#include <cmath>
#include <vector>


Correspondence::Correspondence()
    : m_cutoff(1e-2), m_forceFactor(1.0), m_rhoNoise(5e-6)
{
}

Correspondence::~Correspondence()
{

}

int Correspondence::setCovariance(const double *R)
{
    return m_gaussian.setCoefficient(R);
}

int Correspondence::setCovariance(const double covariance)
{
    return m_gaussian.setCoefficient(covariance);
}

int Correspondence::InitCorrespondence(const int n_status)
{
    m_numVertices = n_status;

    m_isVisible.clear();
	m_isVisible.resize(m_numVertices);

    m_z.clear();

    return 0;
}

void Correspondence::UpdateObservations(const int n_observation, const double *observation)
{
    m_numPoints = n_observation;
    m_pObservation = observation;
}

int Correspondence::ComputeCorrespondence(const double *status)
{
    double *p_cn = new double[m_numPoints];
    for (int n = 0; n < m_numPoints; ++n)
    {
        Eigen::Vector3d c_n(&m_pObservation[3*n]);
        double p_sum = m_rhoNoise;
        for (int k = 0; k < m_numVertices; ++k)
        {
            if (!m_isVisible[k]) continue;
            Eigen::Vector3d m_k(&status[3*k]);
            double r = m_gaussian(c_n - m_k);
            p_sum += r;
        }
        p_cn[n] = p_sum;
    }
    m_z.clear();
    for (int k=0; k < m_numVertices; ++k)
    {
        m_z.push_back(std::vector<IndexValue>());
        if (!m_isVisible[k]) continue;
        Eigen::Vector3d m_k(&status[3*k]);
        for (int n=0; n < m_numPoints; ++n)
        {
            Eigen::Vector3d c_n(&m_pObservation[3*n]);
            double p_cn_mk = m_gaussian(c_n - m_k);
            double z = p_cn_mk / p_cn[n];
            if (z < m_cutoff) continue;
            m_z[k].push_back(IndexValue(n, z));
        }
    }
    delete[] p_cn;
    return 0;
}

int Correspondence::ComputeForce(const double *status, double *force)
{
    memset(force, 0, 3 * m_numVertices * sizeof(double));
    if (m_isVisible.empty()) return -1;
    if (m_z.empty()) return -1;
    for (int k=0; k < m_numVertices; ++k)
    {
        Eigen::Vector3d m_k(&status[3 * k]);
        Eigen::Map<Eigen::Vector3d> f(&force[3 * k]);
        for(unsigned int i=0;i<m_z[k].size();++i)
        {
            Eigen::Vector3d c_n(&m_pObservation[3 * m_z[k][i].index]);
            f += m_z[k][i].value * (m_gaussian.getInverseCovMat() * (c_n - m_k));
        }
    }
   
    for (int k=0; k < m_numVertices * 3; ++k)
        force[k] *= m_forceFactor;
    
    return 0;
}

