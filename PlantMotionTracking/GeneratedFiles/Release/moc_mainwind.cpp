/****************************************************************************
** Meta object code from reading C++ file 'mainwind.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainwind.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwind.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWind_t {
    QByteArrayData data[15];
    char stringdata[248];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_MainWind_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_MainWind_t qt_meta_stringdata_MainWind = {
    {
QT_MOC_LITERAL(0, 0, 8),
QT_MOC_LITERAL(1, 9, 18),
QT_MOC_LITERAL(2, 28, 0),
QT_MOC_LITERAL(3, 29, 19),
QT_MOC_LITERAL(4, 49, 27),
QT_MOC_LITERAL(5, 77, 11),
QT_MOC_LITERAL(6, 89, 13),
QT_MOC_LITERAL(7, 103, 7),
QT_MOC_LITERAL(8, 111, 9),
QT_MOC_LITERAL(9, 121, 19),
QT_MOC_LITERAL(10, 141, 19),
QT_MOC_LITERAL(11, 161, 24),
QT_MOC_LITERAL(12, 186, 7),
QT_MOC_LITERAL(13, 194, 23),
QT_MOC_LITERAL(14, 218, 28)
    },
    "MainWind\0slotLoadConfigFile\0\0"
    "slotBackGroundColor\0slotTestVisibilityDetection\0"
    "slotLoadObj\0slotDoOneStep\0slotRun\0"
    "slotPause\0slotDumpCurrentMesh\0"
    "slotDumpCurrentNode\0slotChangeFlagSaveMeshes\0"
    "checked\0slotChangeFlagSaveNodes\0"
    "slotChangeFlageSaveSnapshots\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWind[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x0a,
       3,    0,   75,    2, 0x0a,
       4,    0,   76,    2, 0x0a,
       5,    0,   77,    2, 0x0a,
       6,    0,   78,    2, 0x0a,
       7,    0,   79,    2, 0x0a,
       8,    0,   80,    2, 0x0a,
       9,    0,   81,    2, 0x0a,
      10,    0,   82,    2, 0x0a,
      11,    1,   83,    2, 0x0a,
      13,    1,   86,    2, 0x0a,
      14,    1,   89,    2, 0x0a,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,

       0        // eod
};

void MainWind::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWind *_t = static_cast<MainWind *>(_o);
        switch (_id) {
        case 0: _t->slotLoadConfigFile(); break;
        case 1: _t->slotBackGroundColor(); break;
        case 2: _t->slotTestVisibilityDetection(); break;
        case 3: _t->slotLoadObj(); break;
        case 4: _t->slotDoOneStep(); break;
        case 5: _t->slotRun(); break;
        case 6: _t->slotPause(); break;
        case 7: _t->slotDumpCurrentMesh(); break;
        case 8: _t->slotDumpCurrentNode(); break;
        case 9: _t->slotChangeFlagSaveMeshes((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->slotChangeFlagSaveNodes((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->slotChangeFlageSaveSnapshots((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject MainWind::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWind.data,
      qt_meta_data_MainWind,  qt_static_metacall, 0, 0}
};


const QMetaObject *MainWind::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWind::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWind.stringdata))
        return static_cast<void*>(const_cast< MainWind*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWind::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
