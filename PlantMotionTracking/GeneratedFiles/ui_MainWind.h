/********************************************************************************
** Form generated from reading UI file 'MainWind.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWIND_H
#define UI_MAINWIND_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include <detectwidget.h>
#include <glviewer.h>

QT_BEGIN_NAMESPACE

class Ui_mainWind
{
public:
    QAction *actionLoadConfig;
    QAction *actionPause;
    QAction *actionRun;
    QAction *actionStep;
    QAction *actionStop;
    QAction *actionBackgroundColor;
    QAction *actionVisibilityTest;
    QAction *actionLoadTest;
    QAction *actionSaveMesh;
    QAction *actionSaveNodes;
    QAction *actionSaveMeshes;
    QAction *actionSaveSnapshots;
    QAction *actionSaveNode;
    QAction *actionLoadShape;
    QAction *actionSaveShape;
    QAction *actionLoadCamera;
    QAction *actionSaveCamera;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    DetectWidget *m_detectWidget;
    GLViewer *m_glViewer;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuControl;
    QMenu *menuView;
    QMenu *menuTest;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *mainWind)
    {
        if (mainWind->objectName().isEmpty())
            mainWind->setObjectName(QStringLiteral("mainWind"));
        mainWind->resize(778, 521);
        actionLoadConfig = new QAction(mainWind);
        actionLoadConfig->setObjectName(QStringLiteral("actionLoadConfig"));
        actionLoadConfig->setCheckable(false);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icon/image/load.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLoadConfig->setIcon(icon);
        actionPause = new QAction(mainWind);
        actionPause->setObjectName(QStringLiteral("actionPause"));
        actionPause->setCheckable(true);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icon/image/pause.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPause->setIcon(icon1);
        actionRun = new QAction(mainWind);
        actionRun->setObjectName(QStringLiteral("actionRun"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icon/image/run.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRun->setIcon(icon2);
        actionStep = new QAction(mainWind);
        actionStep->setObjectName(QStringLiteral("actionStep"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icon/image/do_step.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionStep->setIcon(icon3);
        actionStop = new QAction(mainWind);
        actionStop->setObjectName(QStringLiteral("actionStop"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icon/image/stop.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionStop->setIcon(icon4);
        actionBackgroundColor = new QAction(mainWind);
        actionBackgroundColor->setObjectName(QStringLiteral("actionBackgroundColor"));
        actionBackgroundColor->setCheckable(false);
        actionVisibilityTest = new QAction(mainWind);
        actionVisibilityTest->setObjectName(QStringLiteral("actionVisibilityTest"));
        actionLoadTest = new QAction(mainWind);
        actionLoadTest->setObjectName(QStringLiteral("actionLoadTest"));
        actionSaveMesh = new QAction(mainWind);
        actionSaveMesh->setObjectName(QStringLiteral("actionSaveMesh"));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icon/image/save.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSaveMesh->setIcon(icon5);
        actionSaveNodes = new QAction(mainWind);
        actionSaveNodes->setObjectName(QStringLiteral("actionSaveNodes"));
        actionSaveNodes->setCheckable(true);
        actionSaveNodes->setChecked(false);
        actionSaveMeshes = new QAction(mainWind);
        actionSaveMeshes->setObjectName(QStringLiteral("actionSaveMeshes"));
        actionSaveMeshes->setCheckable(true);
        actionSaveSnapshots = new QAction(mainWind);
        actionSaveSnapshots->setObjectName(QStringLiteral("actionSaveSnapshots"));
        actionSaveSnapshots->setCheckable(true);
        actionSaveNode = new QAction(mainWind);
        actionSaveNode->setObjectName(QStringLiteral("actionSaveNode"));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icon/image/save2.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSaveNode->setIcon(icon6);
        actionLoadShape = new QAction(mainWind);
        actionLoadShape->setObjectName(QStringLiteral("actionLoadShape"));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icon/image/import.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLoadShape->setIcon(icon7);
        actionSaveShape = new QAction(mainWind);
        actionSaveShape->setObjectName(QStringLiteral("actionSaveShape"));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icon/image/export.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSaveShape->setIcon(icon8);
        actionLoadCamera = new QAction(mainWind);
        actionLoadCamera->setObjectName(QStringLiteral("actionLoadCamera"));
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icon/image/camera_load.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLoadCamera->setIcon(icon9);
        actionSaveCamera = new QAction(mainWind);
        actionSaveCamera->setObjectName(QStringLiteral("actionSaveCamera"));
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icon/image/camera_save.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSaveCamera->setIcon(icon10);
        centralWidget = new QWidget(mainWind);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        m_detectWidget = new DetectWidget(centralWidget);
        m_detectWidget->setObjectName(QStringLiteral("m_detectWidget"));

        horizontalLayout->addWidget(m_detectWidget);

        m_glViewer = new GLViewer(centralWidget);
        m_glViewer->setObjectName(QStringLiteral("m_glViewer"));

        horizontalLayout->addWidget(m_glViewer);


        gridLayout->addLayout(horizontalLayout, 0, 0, 1, 1);

        mainWind->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(mainWind);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 778, 23));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuControl = new QMenu(menuBar);
        menuControl->setObjectName(QStringLiteral("menuControl"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QStringLiteral("menuView"));
        menuTest = new QMenu(menuBar);
        menuTest->setObjectName(QStringLiteral("menuTest"));
        mainWind->setMenuBar(menuBar);
        mainToolBar = new QToolBar(mainWind);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        mainToolBar->setIconSize(QSize(36, 36));
        mainToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        mainWind->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(mainWind);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        mainWind->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuControl->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuTest->menuAction());
        menuFile->addAction(actionLoadConfig);
        menuFile->addAction(actionSaveMesh);
        menuFile->addSeparator();
        menuFile->addAction(actionSaveNodes);
        menuFile->addAction(actionSaveMeshes);
        menuFile->addAction(actionSaveSnapshots);
        menuControl->addAction(actionRun);
        menuControl->addAction(actionStep);
        menuControl->addAction(actionPause);
        menuControl->addAction(actionStop);
        menuView->addAction(actionBackgroundColor);
        menuTest->addAction(actionVisibilityTest);
        menuTest->addAction(actionLoadTest);
        mainToolBar->addAction(actionLoadConfig);
        mainToolBar->addAction(actionRun);
        mainToolBar->addAction(actionStep);
        mainToolBar->addAction(actionPause);
        mainToolBar->addAction(actionStop);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionSaveMesh);
        mainToolBar->addAction(actionSaveNode);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionLoadCamera);
        mainToolBar->addAction(actionSaveCamera);

        retranslateUi(mainWind);

        QMetaObject::connectSlotsByName(mainWind);
    } // setupUi

    void retranslateUi(QMainWindow *mainWind)
    {
        mainWind->setWindowTitle(QApplication::translate("mainWind", "MainWind", 0));
        actionLoadConfig->setText(QApplication::translate("mainWind", "LoadConfig", 0));
        actionLoadConfig->setIconText(QApplication::translate("mainWind", "Load", 0));
        actionPause->setText(QApplication::translate("mainWind", "Pause", 0));
        actionRun->setText(QApplication::translate("mainWind", "Run", 0));
        actionStep->setText(QApplication::translate("mainWind", "Step", 0));
        actionStop->setText(QApplication::translate("mainWind", "Stop", 0));
        actionBackgroundColor->setText(QApplication::translate("mainWind", "Background Color", 0));
        actionVisibilityTest->setText(QApplication::translate("mainWind", "VisibilityTest", 0));
        actionLoadTest->setText(QApplication::translate("mainWind", "LoadTest", 0));
        actionSaveMesh->setText(QApplication::translate("mainWind", "Mesh", 0));
        actionSaveNodes->setText(QApplication::translate("mainWind", "Save Nodes", 0));
        actionSaveMeshes->setText(QApplication::translate("mainWind", "Save Meshes", 0));
        actionSaveSnapshots->setText(QApplication::translate("mainWind", "Save Snapshots", 0));
        actionSaveNode->setText(QApplication::translate("mainWind", "Node", 0));
        actionLoadShape->setText(QApplication::translate("mainWind", "LoadShape", 0));
        actionSaveShape->setText(QApplication::translate("mainWind", "SaveShape", 0));
        actionLoadCamera->setText(QApplication::translate("mainWind", "LoadCamera", 0));
        actionSaveCamera->setText(QApplication::translate("mainWind", "SaveCamera", 0));
#ifndef QT_NO_TOOLTIP
        actionSaveCamera->setToolTip(QApplication::translate("mainWind", "SaveCamera", 0));
#endif // QT_NO_TOOLTIP
        menuFile->setTitle(QApplication::translate("mainWind", "File", 0));
        menuControl->setTitle(QApplication::translate("mainWind", "Control", 0));
        menuView->setTitle(QApplication::translate("mainWind", "View", 0));
        menuTest->setTitle(QApplication::translate("mainWind", "Test", 0));
    } // retranslateUi

};

namespace Ui {
    class mainWind: public Ui_mainWind {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWIND_H
