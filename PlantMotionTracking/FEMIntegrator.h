#pragma once

//#include "AbstractIntegrator.h"
#include "VecMatInterface.h"
#include <memory>

namespace VCC
{
    class CorotatedLinearModel;
    class TetMesh;

    class FEMIntegrator //: public AbstractIntegrator
    {
    public:
        FEMIntegrator(CorotatedLinearModel *pModel, TetMesh *tetMesh);
        ~FEMIntegrator(void);

    public:
        void SetGravity(const Vector3d &g);
        void SetDampingParameters(double dampingStiffnessCoef, double dampingMassCoef);
        void SetState(const std::vector<Vector3d> &nodalPosition, const std::vector<Vector3d> &nodalVelocity = std::vector<Vector3d>());
        
        void SymplecticEulerStep(double t);
        bool BackwardEulerStep(double t);

        std::vector<Vector3d> &GetNodalPosition(void) { return m_nodalPosition; }
        const std::vector<Vector3d> &GetNodalVelocity(void) const { return m_nodalVelocity; }
        const std::vector<Vector3d> &GetLastStepNodalForce(void) const { return m_nodalForce; }
        std::vector<Vector3d> &GetLastNodalPosition(void) {return m_nodalPositionLast; }
        const std::vector<Vector3d> &GetExternalForce(void) const { return m_externalForce;}
        const std::vector<Vector3d>  GetNodalDisplacement();

        void ClearInternalForce(void);
        void ClearExternalForce(void);

        void AddAirDragForce(const std::vector<Vector3d> &forces);
        void AddExternalForce(const std::vector<Vector3d> &forces);
        void AddForce(const std::vector<int> &vert, const std::vector<Vector3d> &forces);

        void ConstrainNodesInRange(double range[6], TetMesh *tetMesh);
        void ConstrainNodesByIndex(std::vector<int> nodeIdx);
        void BecomeQuasiStatic();

    private:
        CorotatedLinearModel *m_pModel;
        std::vector<double> m_vertMass;
        TetMesh              *m_pTetMesh;

        int m_integratorId;
        bool m_flagNeedReInitInternalSolver;

        double m_dampingStiffnessCoef;
        double m_dampingMassCoef;
        
        std::vector<Vector3d> m_nodalPositionLast;
        std::vector<Vector3d> m_nodalVelocityLast;

        std::vector<Vector3d> m_nodalPosition;
        std::vector<Vector3d> m_nodalVelocity;
        std::vector<Vector3d> m_nodalForce;

        std::vector<Vector3d> m_externalForce;
        std::vector<Vector3d> m_gravityForce;
        std::vector<Vector3d> m_airForce;
        std::vector<Vector3d> m_zeroForce;

        Vector3d m_gravity;
        std::vector<int> m_constrainedDof;
        std::vector<double> m_constrainedDofPosBuf;
        std::vector<double> m_constrainedDofVelBuf;

        std::vector<int> m_addedForceVert;
        std::vector<Vector3d> m_addedForces;
    };
}