
#include "detectwidget.h"

DetectWidget::DetectWidget(QWidget *parent)
: QGLWidget(parent)
, nodeVisibility(0)
{
  //  setFixedSize(300,300);
}

DetectWidget::~DetectWidget()
{
}

void DetectWidget::initializeGL()
{
    glewInit();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void DetectWidget::paintGL()
{
}

void DetectWidget::resizeGL(int width, int height)
{
    int side = qMin(width, height);
    glViewport((width - side) / 2, (height - side) / 2, side, side);
}

void DetectWidget::PrintVisibility()
{
    int nVertex =nodeVisibility.getVertexOcclusion().size();
    for(int i=0; i<nVertex; i++)
        if(!nodeVisibility.getVertexOcclusion()[i])
            std::cout<<i<<" ";
    std::cout << std::endl;
}

void DetectWidget::DetectVisibility(std::vector<int>& vis, double *p)
{
    // update vertices position of visibility.
    nodeVisibility.updateVertex(p);

    // do detection by virtual paint. 
    makeCurrent();
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPushClientAttrib(GL_ALL_ATTRIB_BITS);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glClearColor(0,0,0,1);
    for(int i=0; i<this->nodeVisibility.getCameraNumber();i++)
    {
        nodeVisibility.initVirtualGL(i);
        nodeVisibility.paintVirtualGL();
    }

    swapBuffers();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPopAttrib();
    glPopClientAttrib();
    doneCurrent();

    // check and output visibility
    //std::vector<int> vectVis = nodeVisibility.checkVisibility();
    //for(int i=0; i<vectVis.size(); i++)
    //	vis[i] = vectVis[i];
    vis = nodeVisibility.checkVisibility();
}

void DetectWidget::InitVisibility(int nVertex, int *pVertexIndex, int nFace, double *dCameraSetting, int nCamera)
{
   // QMutexLocker locker(&mMutex);

    nodeVisibility.initVariable(nVertex, pVertexIndex, nFace, dCameraSetting, nCamera);
}
