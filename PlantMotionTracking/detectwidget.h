#ifndef DETECTWIDGET_H
#define DETECTWIDGET_H

#include "visibility.h"
#include <QGLWidget>
#include <QKeyEvent>
#include <iostream>


class DetectWidget : public QGLWidget
{
    Q_OBJECT
    
public:
	explicit DetectWidget(QWidget *parent = 0);
	~DetectWidget();
	Visibility *vb() {return &nodeVisibility;}

	void DetectVisibility(std::vector<int>& vis, double *p);
    void InitVisibility(int nVertex, int *pVertexIndex, int nFace, double *dCameraSetting, int nCamera);

protected:
	void initializeGL();
	void paintGL();
	void resizeGL(int width, int height);

	void PrintVisibility();

private:
	Visibility		 nodeVisibility;
};

#endif // DEDECTWIDGET_H
