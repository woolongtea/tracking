
#include "visibility.h"

Visibility::Visibility()
{
    vertexIndexOffset = 0;
    isQueryGenerated = true;
    queryID  = NULL;
}

Visibility::Visibility(int f)
{
    vertexIndexOffset = f;
    isQueryGenerated = true;
}

Visibility::~Visibility()
{

}

void Visibility::initVariable(const unsigned int nVertex,const int *pVertexIndex,const unsigned int nFace, const double dCameraSetting[], const int nCamera)
{
    //initialize the coordinates and the visibility of point set
    vertexCoordinate.resize(nVertex);
    vertexVisible.resize(nVertex);
    vertexOcclusion.resize(nVertex);
    for(unsigned int i=0;i<nVertex;i++)
    {
        vertexCoordinate[i].resize(3);
        vertexVisible[i]=0;
        vertexOcclusion[i] = 1;
    }

    //initialize the index information
    vertexIndex.resize(nFace);
    // face_normal_.resize(nFace);
    for(unsigned int i=0;i<nFace;i++)
    {
        vertexIndex[i].resize(3);
        //face_normal_[i].resize(3);
        vertexIndex[i][0]=pVertexIndex[3*i]-vertexIndexOffset;
        vertexIndex[i][1]=pVertexIndex[3*i+1]-vertexIndexOffset;
        vertexIndex[i][2]=pVertexIndex[3*i+2]-vertexIndexOffset;
    }


    //initialize setting of the camera
    cameraSetting.resize(nCamera);
    projectionSetting.resize(nCamera);
    for(int i=0;i<nCamera;i++)
    {
        cameraSetting[i].resize(9);
        projectionSetting[i].resize(6);
    }
    for(unsigned int i=0;i<nCamera;i++)
    {
        for(unsigned int j=0;j<9;j++)
        {
            cameraSetting[i][j] = static_cast<float>(dCameraSetting[i*15+j]);
        }
        for(unsigned int j=0;j<6;j++)
        {
            projectionSetting[i][j] = static_cast<float>(dCameraSetting[i*15+9+j]);
        }
    }

    this->numCamera = nCamera;
}

void Visibility::initVirtualGL(int nCameraIndex)
{
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // Setup the view of the model.
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //gluPerspective(projectionSetting[nCameraIndex][0],projectionSetting[nCameraIndex][1],
    //    projectionSetting[nCameraIndex][2],projectionSetting[nCameraIndex][3]);
    glOrtho(projectionSetting[nCameraIndex][0],projectionSetting[nCameraIndex][1],
            projectionSetting[nCameraIndex][2],projectionSetting[nCameraIndex][3],
            projectionSetting[nCameraIndex][4],projectionSetting[nCameraIndex][5]);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(cameraSetting[nCameraIndex][0],  cameraSetting[nCameraIndex][1], cameraSetting[nCameraIndex][2],     // eye
        cameraSetting[nCameraIndex][3],  cameraSetting[nCameraIndex][4], cameraSetting[nCameraIndex][5],    // look at center
        cameraSetting[nCameraIndex][6],  cameraSetting[nCameraIndex][7], cameraSetting[nCameraIndex][8]);  // Camera up direction
}

void Visibility::paintVirtualGL()
{
    //generate occlusion query id for each vertex
    if(isQueryGenerated)
    {
        queryID = new GLuint[this->vertexCoordinate.size()];
        glGenQueries(vertexCoordinate.size(),this->queryID);
        isQueryGenerated = false;
    }

    int nFace = this->vertexIndex.size();
    int nVertex = this->vertexCoordinate.size();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_LIGHTING);
    //draw occluders first
    glColor3f(1,1,1);
    for(int i=0; i<nFace; i++)
    {
        glBegin(GL_TRIANGLES);
        glVertex3d(vertexCoordinate [vertexIndex[i][0]][0],
            vertexCoordinate [vertexIndex[i][0]][1],
            vertexCoordinate [vertexIndex[i][0]][2]);
        glVertex3d(vertexCoordinate [vertexIndex[i][1]][0],
            vertexCoordinate [vertexIndex[i][1]][1],
            vertexCoordinate [vertexIndex[i][1]][2]);
        glVertex3d(vertexCoordinate [vertexIndex[i][2]][0],
            vertexCoordinate [vertexIndex[i][2]][1],
            vertexCoordinate [vertexIndex[i][2]][2]);
        glEnd();
    }

    //draw Nodes to be queried occlusion
    glColor3f(1,0,0);
    glPointSize(2.0f);
    for(int i=0; i<nVertex; i++)
    {
        glBeginQuery(GL_SAMPLES_PASSED, this->queryID[i]);
        glBegin(GL_POINTS);
        glVertex3d(vertexCoordinate[i][0],vertexCoordinate[i][1],vertexCoordinate[i][2]);
        glEnd();
        glEndQuery(GL_SAMPLES_PASSED);
    }
    glEnable(GL_LIGHTING);
    //query occlusion of each vertex
    GLint  para;
    GLint queryready;
    int count;
    for(int i=0; i<nVertex; i++)
    {
        //queryready = GL_FALSE;
        //count=300000000000;
        //while(!queryready && count--)
        //{
        //    glGetQueryObjectiv(this->queryID[i], GL_QUERY_RESULT_AVAILABLE, &queryready);
        //}
        //if(queryready)
        //{
            glGetQueryObjectiv(this->queryID[i], GL_QUERY_RESULT, &para);
            // if para equals zero,  the vertex is occluded
            if(para != 0)
                this->vertexOcclusion[i] = 0;
        //}
        //else
        //{
        //    vertexOcclusion[i] = 1;
        //}
    }

    if(glDeleteQueries)
        glDeleteQueries(this->vertexCoordinate.size(),queryID);

    isQueryGenerated = true;
    if(queryID)
        delete[] queryID;
}

void Visibility::updateVertex(const double *pVertexCoordinate)
{
    //update the coordinates and the visibility of point set
    unsigned int nVertex = vertexCoordinate.size();
    for(unsigned int i=0;i<nVertex;i++)
    {
        //vertexCoordinate[i][0]=static_cast<float>(pVertexCoordinate[3*i]);
        //vertexCoordinate[i][1]=static_cast<float>(pVertexCoordinate[3*i+1]);
        //vertexCoordinate[i][2]=static_cast<float>(pVertexCoordinate[3*i+2]);
        vertexCoordinate[i][0]=pVertexCoordinate[3*i];
        vertexCoordinate[i][1]=pVertexCoordinate[3*i+1];
        vertexCoordinate[i][2]=pVertexCoordinate[3*i+2];
        vertexVisible[i]=0;
        vertexOcclusion[i] =1;
    }
}

std::vector<int> Visibility::checkVisibility()
{
    int nVertex = vertexCoordinate.size();
    for(int i=0; i<nVertex; i++)
        vertexVisible[i] = 1-vertexOcclusion[i];
    return this->vertexVisible;
}

std::vector<int> Visibility::getVertexVisibility()
{
    return this->vertexVisible;
}

std::vector<int> Visibility::getVertexOcclusion()
{
    return this->vertexOcclusion;
}
