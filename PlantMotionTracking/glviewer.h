#ifndef GLVIEWER_H
#define GLVIEWER_H

#include "QGLViewer/qglviewer.h"
#include "comHeader.h"
#include "GLTetMesh.h"
#include "FineMesh.h"
#include <QKeyEvent>
#include <QtOpenGL/QGLShaderProgram>
#include <vector>
#include <memory>

class GLViewer : public QGLViewer
{
public:
    GLViewer(QWidget *parent);
    ~GLViewer(void);
    //void SetVisibility(std::vector<int>& v) {test_v = v;}
    //void SetMesh(std::shared_ptr<TriMeshType> m) {m_glMesh = m;}
    void SetTetMesh(std::shared_ptr<GLTetMesh> tetMesh) {m_glTetMesh = tetMesh;}
    void SetFineMesh(std::shared_ptr<FineMesh> fineMesh) {m_glFineMesh = fineMesh;}
    void SetPointCloud(std::vector<double> pc) {m_glPointCloud = pc;}
    std::shared_ptr<GLTetMesh> GetGLTetMesh() {return m_glTetMesh;}
    void SetupProjViewModelMatrixForShader(QGLShaderProgram &shader);
    void SetupLightsForShader(QGLShaderProgram &shader);
    void ClearTetMesh();

protected:
    virtual void init();
    virtual void draw();
    virtual void keyPressEvent(QKeyEvent *e);
    virtual void postDraw();

private:
    void DrawTestBox();
    void DrawMesh();
    void DrawCornerAxis();
    void DrawTetMesh();
    void DrawFineMesh();
    void DrawPointCloud();

private:
    //std::vector<int> test_v;
    bool m_flagTetFrame;
    bool m_flagPC;
    //std::shared_ptr<TriMeshType> m_glMesh;
    
    // Tet Mesh
    std::shared_ptr<GLTetMesh> m_glTetMesh;
    std::shared_ptr<FineMesh>  m_glFineMesh;
    std::vector<double>        m_glPointCloud;
};

#endif 