#ifndef MAINWIND_H
#define MAINWIND_H

#include "detectwidget.h"
#include "ui_MainWind.h"
#include "comHeader.h"
#include "TetMesh.h"
#include "FineMesh.h"
#include "CorotatedLinearModel.h"
#include "CorotatedLinearModelMultiThreaded.h"
#include "FEMIntegrator.h"
#include "correspondence.h"
#include <QtWidgets/QMainWindow>
#include <QDockWidget>
#include <QFileDialog>
#include <QColorDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QLabel>
#include <QTimer>
#include <QDir>
#include <memory>
#include <string>
#include <sstream>

using namespace VCC;

class MainWind : public QMainWindow
{
    Q_OBJECT

public:
    MainWind(QWidget *parent = 0);
    ~MainWind();

    public slots:
        void slotLoadConfigFile();
        void slotBackGroundColor();
        void slotTestVisibilityDetection();
        void slotLoadObj();
        void slotDoOneStep();
        void slotRun();
        void slotPause();
        void slotDumpCurrentMesh();
        void slotDumpCurrentNode();
        void slotChangeFlagSaveMeshes(bool checked);
        void slotChangeFlagSaveNodes(bool checked);
        void slotChangeFlageSaveSnapshots(bool checked);

private:
    void LoadConfig(const QString &confFileName);
    void InitConnections();
    void InitPointClouds();
    void InitMesh();
    void InitModel();
    void InitIntegrator();
    void InitTracking();
    void DoOneStep();

private:
    Ui::mainWind ui;

    QTimer m_idleTimer;
    int    m_currentFrame;
    qint64 m_trackStartWorldTime;
    qint64 m_trackTotalTime;
    bool   m_flagRun;
    bool   m_flagSaveMeshes;
    bool   m_flagSaveNodes;
    bool   m_flagSaveSnapshots;

/*******************config value***********************************/
    QMap<QString, QString> m_configs;
    QString m_configFile;
    QString m_configPath;
    Vector3d m_gravity;
    int m_pcStartIdx;
    int m_pcEndIdx;
    int m_pcIdxStep;
    int m_maxLoopNum;
    int m_loopCount;
    double m_covariance;
    double m_forceFactor;
    double m_stableThreshold;
    std::vector<double> m_cameraSet;
    double m_timeStep;
    bool   m_flagQuasiStatic;
    bool   m_flagResetShapeEveryFrame;

/******************model data*****************************************/
    std::shared_ptr<FineMesh> m_fineMesh;
    std::shared_ptr<TetMesh> m_tetMesh;
    std::shared_ptr<CorotatedLinearModel> m_model;
    std::shared_ptr<FEMIntegrator> m_integrator;
    std::vector<Vector3d> m_resetShape;
    std::vector<std::vector<double>> m_pointcloudsBuffer;

    Correspondence m_correspondence;
};

#endif // MAINWIND_H
