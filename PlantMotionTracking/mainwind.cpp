#include "mainwind.h"

#include <QtCore\QString>
#include <numeric>
#include <functional>
#include <iterator>

using namespace VCC;

MainWind::MainWind(QWidget *parent)
    : QMainWindow(parent)
    , m_currentFrame(0)
    , m_trackStartWorldTime(0)
    , m_trackTotalTime(0)
    , m_maxLoopNum(200)
    , m_loopCount(0)
    , m_flagRun(false)
    , m_flagQuasiStatic(false)
    , m_flagSaveMeshes(false)
    , m_flagResetShapeEveryFrame(false)
{
    ui.setupUi(this);
    InitConnections();
    m_fineMesh = std::make_shared<FineMesh>(ui.m_glViewer);
    ui.m_glViewer->SetFineMesh(m_fineMesh);
}

MainWind::~MainWind()
{
}

void MainWind::InitConnections()
{
    connect(ui.actionLoadConfig,SIGNAL(triggered()),this,SLOT(slotLoadConfigFile()));
    connect(ui.actionBackgroundColor, SIGNAL(triggered()),this, SLOT(slotBackGroundColor()));
    connect(ui.actionVisibilityTest,SIGNAL(triggered()),this,SLOT(slotTestVisibilityDetection()));
    connect(ui.actionLoadTest,SIGNAL(triggered()),this,SLOT(slotLoadObj()));
    connect(ui.actionStep,SIGNAL(triggered()),this,SLOT(slotDoOneStep()));
    connect(ui.actionRun,SIGNAL(triggered()),this,SLOT(slotRun()));
    connect(&m_idleTimer, SIGNAL(timeout()), this, SLOT(slotDoOneStep()));
    connect(ui.actionPause,SIGNAL(triggered()),this,SLOT(slotPause()));
    connect(ui.actionSaveMesh,SIGNAL(triggered()),this,SLOT(slotDumpCurrentMesh()));
    connect(ui.actionSaveMeshes,SIGNAL(toggled(bool)),this,SLOT(slotChangeFlagSaveMeshes(bool)));
    connect(ui.actionSaveNodes,SIGNAL(toggled(bool)),this,SLOT(slotChangeFlagSaveNodes(bool)));
    connect(ui.actionSaveSnapshots,SIGNAL(toggled(bool)),this,SLOT(slotChangeFlageSaveSnapshots(bool)));
    connect(ui.actionSaveNode,SIGNAL(triggered()),this,SLOT(slotDumpCurrentNode()));
}

void MainWind::slotLoadConfigFile()
{
    QFileDialog *fileDialog = new QFileDialog(this);
    fileDialog->setWindowTitle(tr("Load tracking config file."));
    fileDialog->setNameFilter(QString("Config(*.config)"));

    if(fileDialog->exec() == QDialog::Accepted)
    {
        QString confFileName = fileDialog->selectedFiles()[0];
        if (confFileName.isEmpty())
            return;
        LoadConfig(confFileName);
        ui.m_glViewer->update();
    }
}

void MainWind::slotBackGroundColor()
{
    QColor color = QColorDialog::getColor(Qt::black, this,"Choose Background Color"); 
    ui.m_glViewer->setBackgroundColor(color);
}

void MainWind::slotTestVisibilityDetection()
{
    ui.m_detectWidget->DetectVisibility(m_fineMesh->GetVisibility(),
        m_fineMesh->GetVerticesBuffer().data());
    ui.m_glViewer->update();
}

void MainWind::slotLoadObj()
{
    /***************** Integrator and Rendering Test **********************
    std::vector<Vector3d> test_force;
    test_force.resize(m_tetMesh->GetVertices().size());
    std::fill(test_force.begin(),test_force.end(),Vector3d::ZERO);
    test_force[480] = Vector3d(0,1,0);
    m_integrator->ClearExternalForce();
    m_integrator->AddExternalForce(test_force);
    m_integrator->BackwardEulerStep(0.05);
    m_tetMesh->GetVertices() = m_integrator->GetNodalPosition();
    m_fineMesh->UpdateVerticesByInterpolating(m_tetMesh->GetVertices());


    ui.m_glViewer->GetGLTetMesh()->SetVertForce(
    reinterpret_cast<const double *>(m_integrator->GetLastStepNodalForce().data()),
    1.0 / m_tetMesh->GetMass() * m_tetMesh->GetVertices().size());

    ui.m_glViewer->update();
    **********************************************************************/

    //auto camera_position = ui.m_glViewer->camera()->position();
    //std::cout << "camera position: "<<camera_position[0] <<" "<< camera_position[1] <<" "<< camera_position[2] << std::endl;

    QFileDialog *fileDialog = new QFileDialog(this);
    fileDialog->setWindowTitle(tr("Load test obj file."));
    fileDialog->setNameFilter(QString("Obj(*.obj)"));

    if(fileDialog->exec() == QDialog::Accepted)
    {
        QString confFileName = fileDialog->selectedFiles()[0];
        if (confFileName.isEmpty())
            return;
        m_fineMesh->LoadFineMeshFromFile(confFileName.toStdString());
        
    }

  // for (int i=0;i<726;++i)
   for(int i=726; i<11528; ++i)
   {
       m_fineMesh->GetOpenMesh()->delete_vertex(m_fineMesh->GetOpenMesh()->vertex_handle(i));
   }
   
   m_fineMesh->GetOpenMesh()->garbage_collection();
   ui.m_glViewer->updateGL();
}

void MainWind::slotDoOneStep()
{
    DoOneStep();
}

void MainWind::slotRun()
{ 
    if (!m_flagRun)
    {
        m_flagRun = true;
        m_idleTimer.start(0);
        m_trackStartWorldTime = QDateTime::currentMSecsSinceEpoch();
    }
}

void MainWind::slotPause()
{
    if (!m_tetMesh)
        return;

    if (!m_flagRun)
    {
        slotRun();
        return;
    }

    m_idleTimer.stop();
    m_trackTotalTime += QDateTime::currentMSecsSinceEpoch() - m_trackStartWorldTime;
    m_flagRun = false;
}

void MainWind::LoadConfig( const QString &confFileName )
{
    QFile confFile(confFileName);
    if (!confFile.open(QIODevice::ReadOnly))
    {       
        QMessageBox::warning(this, tr("Error!"), tr("Fail to open config file"));
        return;
    }

    QTextStream fin(&confFile);
    QString firstLine = fin.readLine().simplified();
    if (firstLine != QStringLiteral("!TrackingConfig"))
    {
        QMessageBox::warning(this, tr("Error!"), confFileName + tr(" is not a config file"));
        return;
    }

    m_configs.clear();
    while (!fin.atEnd())
    {
        QString line = fin.readLine();
        line = line.simplified();
        if (line[0] == QChar('#'))
            continue;

        if (line.isEmpty() || line[0].isSpace())
            continue;

        QStringList segs = line.split(':');
        if (segs.size() < 2)
            continue;
        m_configs[segs[0].toLower().simplified()] = segs[1].simplified();
    }

    m_configFile = confFileName;

    QString path = QDir::toNativeSeparators(m_configFile);
    m_configPath = path.left(path.lastIndexOf('\\') + 1);

    // gravity  
    m_gravity = Vector3d(0, -9.8, 0);
    QString gstr = m_configs.value(QStringLiteral("gravity"));
    if (!gstr.isEmpty())
    {
        std::wstringstream(gstr.utf16()) >> m_gravity.X() >> m_gravity.Y() >> m_gravity.Z();
    }

    InitMesh();
    InitModel();
    InitIntegrator();
    InitPointClouds();
    InitTracking();
    //InitMisc();
}

void MainWind::InitMesh()
{
    bool success = false;
    // tet mesh
    m_tetMesh.reset(new TetMesh());

    if (!m_tetMesh->LoadElements((m_configPath + m_configs.value(QStringLiteral("elements"))).utf16()))
        return;
    if (!m_tetMesh->LoadVertices((m_configPath + m_configs.value(QStringLiteral("vertices"))).utf16()))
        return;

    // load material information
    int ctrlPointsNum = m_configs.value(QStringLiteral("ctrlpointnum")).toInt(&success);
    if(!success)
        ctrlPointsNum = 1;

    auto EList = m_configs.value(QStringLiteral("youngsmodulus")).split(" ");
    auto nuList = m_configs.value(QStringLiteral("possionsratio")).split(" ");
    double density = m_configs.value(QStringLiteral("density")).toDouble(&success);
    if (!success)
        density = 1000.0;

    std::vector<double> vecE;
    std::vector<double> vecNu;
    std::vector<double> vecWeight;

    for(int i=0; i<EList.size(); i++)
    {
        vecE.push_back(EList[i].toDouble(&success));
        vecNu.push_back(nuList[i].toDouble(&success));
    }

    if(ctrlPointsNum != 1)
    {
        QString weighPath = m_configPath + m_configs.value(QStringLiteral("weight"));

        tifstream fin(weighPath.utf16());
        if (!fin.is_open())
        {
            std::tcout << TEXT("Can't open ") << weighPath.utf16() << std::endl;
            return;
        }

        tstring line;
        while (std::getline(fin, line))
        {
            tistringstream sstr(line);
            double tmpValue;
            for(int j=0; j<ctrlPointsNum; j++)
            {
                sstr >> tmpValue;
                vecWeight.push_back(tmpValue);
            }
        }
    }

    m_tetMesh->SetMaterial(vecWeight, vecE, vecNu, density);

    //double E = m_configs.value(QStringLiteral("youngsmodulus")).toDouble(&success);
    //if (!success)
    //	E = 1e7;
    //double nu = m_configs.value(QStringLiteral("possionsratio")).toDouble(&success);
    //if (!success)
    //	nu = 0.45;
    //double density = m_configs.value(QStringLiteral("density")).toDouble(&success);
    //if (!success)
    //	density = 1000.0;
    //m_tetMesh->SetUniformMaterial(E, nu, density);

    m_tetMesh->ComputeVolumeMass();

    if (!m_configs.value(QStringLiteral("nodalmass")).isEmpty())
        m_tetMesh->LoadNodalMass((m_configPath + m_configs.value(QStringLiteral("nodalmass"))).utf16());
    ui.statusBar->addWidget(new QLabel(tr("Node:%1  Tet:%2  E:%3  nu:%4")
        .arg(m_tetMesh->GetVertices().size())
        .arg(m_tetMesh->GetElements().size())
        .arg(vecE[0]).arg(vecNu[0])));

    ui.m_glViewer->ClearTetMesh();
    ui.m_glViewer->SetTetMesh(std::make_shared<GLTetMesh>(ui.m_glViewer, m_tetMesh));

    if (!m_configs.value(QStringLiteral("resetshape")).isEmpty())
    {
        TetMeshVertices::LoadVertices(
            (m_configPath + m_configs.value(QStringLiteral("resetshape"))).utf16(), 
            m_resetShape);
    }
    else
        m_resetShape = m_tetMesh->GetVertices();

    m_fineMesh->LoadFineMeshFromFile(
        (m_configPath + m_configs.value(QStringLiteral("finemesh"))).toStdString());
    m_fineMesh->LoadInterpMatFromFile(m_configPath + m_configs.value(QStringLiteral("interpmat")));
    //m_fineMesh->UpdateVerticesByInterpolating(m_tetMesh->GetVertices());
}

void MainWind::InitModel()
{
    bool success = false;
    // modal
    int nthread = m_configs.value(QStringLiteral("nthread")).toInt(&success);
    if (!success)
    {
        nthread = QThread::idealThreadCount();
        //printf("%d\n", nthread);
    }

    m_model.reset(new CorotatedLinearModelMultiThreaded(m_tetMesh->GetTopology(), nthread));
    //m_model.reset(new CorotatedLinearModel(m_tetMesh.get()));

    m_model->InitializeFirstPiolaKirchhoffMethod(m_tetMesh.get());
    m_model->InitializeUndeformedStiffnessMatrix(m_tetMesh.get());
}

void MainWind::InitIntegrator()
{
    bool success = false;
    // integrator
    double dampingStiffnessCoef = m_configs.value(QStringLiteral("dampingstiffnesscoef")).toDouble(&success);
    if (!success)
        dampingStiffnessCoef = 0.0;
    double dampingMassCoef = m_configs.value(QStringLiteral("dampingmasscoef")).toDouble(&success);
    if (!success)
        dampingMassCoef = 0.0;

    m_integrator.reset(new FEMIntegrator(m_model.get(), m_tetMesh.get())/*, m_restShapeProblem)*/);
    m_integrator->SetDampingParameters(dampingStiffnessCoef, dampingMassCoef);
    m_integrator->SetGravity(m_gravity);

    auto constrainedRange = m_configs.value(QStringLiteral("constrainedrange"));
    if (!constrainedRange.isEmpty())
    {
        auto rangeStrList = constrainedRange.split(' ');
        double range[6] = { 
            rangeStrList.value(0).toDouble(), rangeStrList.value(1).toDouble(), rangeStrList.value(2).toDouble(),
            rangeStrList.value(3).toDouble(), rangeStrList.value(4).toDouble(), rangeStrList.value(5).toDouble()
        };

        m_integrator->ConstrainNodesInRange(range, m_tetMesh.get());
    }

    auto constrainedIdsStr = m_configs.value(QStringLiteral("constrainednodes"));
    if (!constrainedIdsStr.isEmpty())
    {
        auto rangeStrList = constrainedIdsStr.split(' ');
        std::set<int> nodeset;
        for (auto &str : rangeStrList)
        {
            int id = str.toInt(&success);
            if (success)
                nodeset.insert(id);
        }

        std::vector<int> nodes(nodeset.begin(), nodeset.end());
        //std::vector<int> nodes(++nodeset.begin(), nodeset.end());
        //nodes.push_back(*nodeset.begin());
        //std::copy(nodes.begin(), nodes.end(), std::ostream_iterator<int>(std::cout, "\t"));
        //std::cout << std::endl;
        //if (!nodes.empty())
        //{
        //	for (auto &id : nodes)
        //		id += nodes.back();
        //	nodes.pop_back();
        //}
        m_integrator->ConstrainNodesByIndex(nodes);
    }

    m_timeStep=m_configs.value(QStringLiteral("timestep")).toDouble();
}

void MainWind::InitPointClouds()
{
    m_pcStartIdx = m_configs.value(QStringLiteral("startidx")).toInt();
    m_pcEndIdx   = m_configs.value(QStringLiteral("endidx")).toInt();
    m_pcIdxStep  = m_configs.value(QStringLiteral("idxstep")).toInt();

    int numFrames = std::abs(m_pcEndIdx-m_pcStartIdx) + 1;
    m_pointcloudsBuffer.resize(numFrames);

    std::cout<<"Loading Point Cloud: ";

    for (int i = 0; i < numFrames; ++i)
    {
        m_pointcloudsBuffer[i].clear();
        QString pcFileName = m_configPath 
            + m_configs.value(QStringLiteral("prefix"))
            + QString::number(m_pcStartIdx + i*m_pcIdxStep,10)
            + m_configs.value(QStringLiteral("suffix"));

        QFile pcFile(pcFileName);
        if (!pcFile.open(QIODevice::ReadOnly))
        {       
            QMessageBox::warning(this, tr("Error!"), tr("Fail to open point cloud file"));
            return;
        }

        QTextStream fin(&pcFile);
        while (!fin.atEnd())
        {
            QString line = fin.readLine();
            line = line.simplified();
            if (line[0] != QChar('v'))
                continue;

            if (line.isEmpty() || line[0].isSpace())
                continue;

            QStringList segs = line.split(' ');
            m_pointcloudsBuffer[i].push_back(segs[1].toDouble());
            m_pointcloudsBuffer[i].push_back(segs[2].toDouble());
            m_pointcloudsBuffer[i].push_back(segs[3].toDouble());
        }
        std::cout<<" "<<m_pcStartIdx + i*m_pcIdxStep;
    }
    std::cout<<std::endl;
    ui.m_glViewer->SetPointCloud(m_pointcloudsBuffer[m_currentFrame]);
}

void MainWind::InitTracking()
{
    int num_camera =  m_configs.value(QStringLiteral("cameranum")).toInt();
    if (num_camera>0)
    {
        QStringList lookat = m_configs.value(QStringLiteral("lookatone")).split(" ");
        for (int i=0;i<9;++i)
        {
            m_cameraSet.push_back(lookat[i].toDouble());
        }
        QStringList perspective = m_configs.value(QStringLiteral("orthoone")).split(" ");
        for (int i=0;i<6;++i)
        {
            m_cameraSet.push_back(perspective[i].toDouble());
        }
    }
    if (num_camera>1)
    {
        QStringList lookat = m_configs.value(QStringLiteral("lookattwo")).split(" ");
        for (int i=0;i<9;++i)
        {
            m_cameraSet.push_back(lookat[i].toDouble());
        }
        QStringList perspective = m_configs.value(QStringLiteral("orthotwo")).split(" ");
        for (int i=0;i<6;++i)
        {
            m_cameraSet.push_back(perspective[i].toDouble());
        }
    }
    if (num_camera>2)
    {
        QStringList lookat = m_configs.value(QStringLiteral("lookatthree")).split(" ");
        for (int i=0;i<9;++i)
        {
            m_cameraSet.push_back(lookat[i].toDouble());
        }
        QStringList perspective = m_configs.value(QStringLiteral("orthothree")).split(" ");
        for (int i=0;i<6;++i)
        {
            m_cameraSet.push_back(perspective[i].toDouble());
        }
    }

    ui.m_detectWidget->InitVisibility(m_fineMesh->GetVerticesNum(),
        m_fineMesh->GetFaceVertexIndices().data(),
        m_fineMesh->GetFacesNum(),
        m_cameraSet.data(),
        num_camera);

    m_correspondence.InitCorrespondence(m_fineMesh->GetVerticesNum());
    m_forceFactor = m_configs.value(QStringLiteral("forcefactor")).toDouble();
    m_correspondence.setForceFactor(m_forceFactor);
    m_covariance = m_configs.value(QStringLiteral("covariance")).toDouble();
    m_correspondence.setCovariance(m_covariance);

    m_stableThreshold = m_configs.value(QStringLiteral("stablethr")).toDouble();

    if (m_configs.value(QStringLiteral("quasistatic"))==QStringLiteral("true"))
        m_flagQuasiStatic = true;
    else
        m_flagQuasiStatic = false;  

    if (m_configs.value(QStringLiteral("resetshapeeveryframe"))==QStringLiteral("true"))
        m_flagResetShapeEveryFrame = true;
    else
        m_flagResetShapeEveryFrame = false;
    
}

void MainWind::DoOneStep()
{
    if (m_currentFrame<m_pointcloudsBuffer.size())
    {
        ui.m_detectWidget->DetectVisibility(m_fineMesh->GetVisibility(),
                                            m_fineMesh->GetVerticesBuffer().data());
        m_correspondence.setVisibility(m_fineMesh->GetVisibility());
        m_correspondence.UpdateObservations(m_pointcloudsBuffer[m_currentFrame].size()/3,m_pointcloudsBuffer[m_currentFrame].data());
        auto finemesh_pos = m_fineMesh->GetVerticesBuffer();
        m_correspondence.ComputeCorrespondence(finemesh_pos.data());
        m_correspondence.ComputeForce(finemesh_pos.data(),
            reinterpret_cast<double *>(m_fineMesh->GetTrackFineForces().data()));
        auto ff = m_fineMesh->GetTrackFineForces();
        std::vector<Vector3d> tetForces(m_tetMesh->GetVertices().size());
        m_fineMesh->MapFineForces2CoarseForces(tetForces);
        m_integrator->ClearExternalForce();
        m_integrator->AddExternalForce(tetForces);
        if (m_flagQuasiStatic)   
            m_integrator->BecomeQuasiStatic();
        m_integrator->BackwardEulerStep(m_timeStep);

        auto disp = m_integrator->GetNodalDisplacement();
        Eigen::Map<Eigen::VectorXd> disp_buffer(reinterpret_cast<double *>(disp.data()),
            disp.size()*3);
        double max_disp = disp_buffer.lpNorm<Eigen::Infinity>();
        m_tetMesh->GetVertices() = m_integrator->GetNodalPosition();
        m_model->InitializeUndeformedStiffnessMatrix(m_tetMesh.get());

        m_fineMesh->UpdateVerticesByInterpolating(m_tetMesh->GetVertices());
        ui.m_glViewer->GetGLTetMesh()->SetVertForce(
            reinterpret_cast<const double *>(tetForces.data()),
            0.05 / m_tetMesh->GetMass() * m_tetMesh->GetVertices().size());
        ui.m_glViewer->SetPointCloud(m_pointcloudsBuffer[m_currentFrame]);
        ui.m_glViewer->updateGL();
        ++m_loopCount;
        if (max_disp<m_stableThreshold || m_loopCount > m_maxLoopNum)
        {
            std::cout<<"finish tracking point cloud frame: "<< m_pcStartIdx + m_currentFrame*m_pcIdxStep  << std::endl;

            if (m_flagSaveSnapshots)
            {
                QString snap_file = QString(tr("snapshot/snapshot_%1")
                    .arg(m_pcStartIdx + m_currentFrame*m_pcIdxStep))+QStringLiteral(".png");
                ui.m_glViewer->saveSnapshot(snap_file,true);
            }

            if (m_flagSaveNodes)
            {
	            QString vert_file = QString(tr("node/dumpNode_%1")
	                .arg(m_pcStartIdx + m_currentFrame*m_pcIdxStep))+QStringLiteral(".node");
	            m_tetMesh->DumpNode(vert_file.toStdString());
            }

            if (m_flagSaveMeshes)
            {
	            QString obj_file = QString(tr("obj/dumpObj_%1")
	                .arg(m_pcStartIdx + m_currentFrame*m_pcIdxStep))+QStringLiteral(".obj");
	            m_fineMesh->DumpCurrentMesh(obj_file.toStdString());
            }

            ++m_currentFrame;
            m_loopCount = 0;

            if (m_flagResetShapeEveryFrame)
            {
                m_fineMesh->UpdateVerticesByInterpolating(m_resetShape);
                m_tetMesh->GetVertices() = m_resetShape;
                m_integrator->GetNodalPosition() = m_integrator->GetLastNodalPosition() = m_resetShape;
            }
        }
    } 
    else
    {
        std::cout<<"finish tracking all frames"<<std::endl;
        m_idleTimer.stop();
        m_trackTotalTime += QDateTime::currentMSecsSinceEpoch() - m_trackStartWorldTime;
        double used_time = static_cast<double>(m_trackTotalTime) / 1000.0;
        std::cout<<"used time:" << used_time <<"s"<<std::endl;
    }
}

void MainWind::slotDumpCurrentMesh()
{
    if (m_fineMesh->GetVerticesNum()!=0)
    {
        QString meshFileName = QFileDialog::getSaveFileName(this, 
            tr("Save Obj File"), m_configPath+QStringLiteral("untitled.obj"), 
            tr("Obj files (*.obj)"));

        if (meshFileName.isEmpty())
            return;
        m_fineMesh->DumpCurrentMesh(meshFileName.toStdString());
    }
    else
    {
        std::cerr<<"no mesh to dump!"<<std::endl;
    }
}

void MainWind::slotChangeFlagSaveMeshes( bool checked )
{
    m_flagSaveMeshes = checked;
    if (m_flagSaveMeshes)
    {
        QDir dir;
        if (!dir.exists(QString("obj")))
        {
            dir.mkpath(QString("obj"));
        }
    }
}

void MainWind::slotChangeFlagSaveNodes( bool checked )
{
    m_flagSaveNodes = checked;
    if (m_flagSaveNodes)
    {
        QDir dir;
        if (!dir.exists(QString("node")))
        {
            dir.mkpath(QString("node"));
        }
    }
}

void MainWind::slotChangeFlageSaveSnapshots( bool checked )
{
    m_flagSaveSnapshots = checked;
    if (m_flagSaveSnapshots)
    {
        QDir dir;
        if (!dir.exists(QString("snapshot")))
        {
            dir.mkpath(QString("snapshot"));
        }
    }
}

void MainWind::slotDumpCurrentNode()
{
    if (m_tetMesh->GetVertices().size()!=0)
    {
        QString nodeFileName = QFileDialog::getSaveFileName(this, 
            tr("Save Node File"), m_configPath+QStringLiteral("untitled.node"), 
            tr("Obj files (*.node)"));

        if (nodeFileName.isEmpty())
            return;
        m_tetMesh->DumpNode(nodeFileName.toStdString());
    }
    else
    {
        std::cerr<<"no Tet mesh to dump!"<<std::endl;
    }
}
