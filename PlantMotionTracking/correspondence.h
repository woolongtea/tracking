#ifndef CORRESPONDENCE_H
#define CORRESPONDENCE_H

#include "gaussiandistribution.h"

typedef struct _IndexValue
{
	_IndexValue(int i, double v) : index(i), value(v) {}
	int index;
	double value;
} IndexValue;

typedef std::vector<std::vector<IndexValue> > SparseArray;

class Correspondence
{
public:
    explicit Correspondence();
    ~Correspondence();

    int setCovariance(const double *R);
    int setCovariance(const double covariance);


    void setForceFactor(const double factor) { m_forceFactor = factor; }
    void setNoise(const double noise) { m_rhoNoise = noise; }
    void setVisibility(const std::vector<int> & v) {m_isVisible=v;}

    int InitCorrespondence(const int n_status);
    int ComputeCorrespondence(const double *status);
    int ComputeForce(const double *status, double *force);
    void UpdateObservations(const int n_observation,const double *observation);

public:
    GaussianDistribution m_gaussian;
    SparseArray m_z;
    std::vector<int> m_isVisible;
    int m_numPoints;
	int m_numVertices;
    const double *m_pObservation;
    double m_cutoff;
    double m_forceFactor;
    double m_rhoNoise;
};

#endif // CORRESPONDENCE_H
