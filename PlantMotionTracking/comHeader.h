#ifndef COMHEADER_H
#define  COMHEADER_H

#define _USE_MATH_DEFINES
#include "VecMatInterface.h"
#include "Eigen/Dense"
#include "Eigen/LU"
#include "Eigen/Core"
#include "OpenMesh/Core/IO/MeshIO.hh"
#include "OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh"
struct MyTraits : OpenMesh::DefaultTraits
{
    typedef OpenMesh::Vec3f Point;
    typedef OpenMesh::Vec3f Normal;
    typedef OpenMesh::Vec2f  TexCoord;

    VertexAttributes(OpenMesh::Attributes::Status);
    EdgeAttributes(OpenMesh::Attributes::Status);
    FaceAttributes(OpenMesh::Attributes::Status);
};
typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> TriMeshType;

#endif

