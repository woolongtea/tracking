#ifndef VISIBILITY_H
#define VISIBILITY_H

#include "GL/glew.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <string>

class Visibility
{
public:
    Visibility();
    Visibility(int f);
     ~Visibility();
    void initVariable(const unsigned int nVertex,const int *pVertexIndex,const unsigned int nFace, const double dCameraSetting[],const int nCamera);
    void updateVertex(const double* pVertexCoordinate);
    void initVirtualGL(int nCameraIndex);
    void paintVirtualGL();
    int  getCameraNumber() {return numCamera;}

    std::vector<int> checkVisibility();
    std::vector<int> getVertexVisibility();
    std::vector<int> getVertexOcclusion();


private:
    //camera setting of gluLookat;
    std::vector< std::vector<float> > cameraSetting;
    std::vector< std::vector<float> > projectionSetting;
    std::vector< std::vector<double> > vertexCoordinate;
    std::vector< std::vector<int> > vertexIndex;
    std::vector<int> vertexOcclusion;
    std::vector<int> vertexVisible;
    int vertexIndexOffset;
    int numCamera;
    GLuint *queryID;
    bool isQueryGenerated;

};

#endif //end VISIBILITY_H
